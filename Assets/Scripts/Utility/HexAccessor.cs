﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HexAccessor : MonoBehaviour
{
    public static float Distance(Hex a, Hex b)
    {
        int dQ = Mathf.Abs(a.Q - b.Q);
        int dR = Mathf.Abs(a.R - b.R);
        return
            Mathf.Max(
                dQ,
                dR,
                Mathf.Abs(a.S - b.S)
            );
    }

    /// <summary>
    /// Returns list of hexes in radius of selected hex
    /// </summary>
    /// <param name="centerHex">Hex in the center of radius</param>
    /// <param name="radious">Radius of search</param>
    /// <returns></returns>
    public static List<HexComponent> GetHexesAround(HexComponent centerHex, int radius)
    {
        List<HexComponent> hexes = new List<HexComponent>();

        foreach (HexComponent hex in centerHex.map.hexes)
        {
            if (Distance(hex.hex, centerHex.hex) <= radius)
            {
                hexes.Add(hex);
            }
        }
        return hexes;
    }

    /// <summary>
    /// Returns list of hexes on the edge of radius
    /// </summary>
    /// <param name="centerHex">Hex in the center of radius</param>
    /// <param name="radius">Radius of search</param>
    /// <returns></returns>
    public static List<HexComponent> GetRingAround(HexComponent centerHex, int radius)
    {
        List<HexComponent> hexes = new List<HexComponent>();

        foreach (HexComponent hex in centerHex.map.hexes)
        {
            if (Distance(hex.hex, centerHex.hex) == radius)
            {
                hexes.Add(hex);
            }
        }
        return hexes;
    }

    /// <summary>
    /// Returns list of hexes in radius of selected hex
    /// </summary>
    /// <param name="centerHex">Hex in the center of radius</param>
    /// <param name="radius">Radius of search</param>
    /// <returns></returns>
    public static List<HexComponent> GetMovementHexes(Unit unit)
    {
        List<HexComponent> aroundHexes = GetHexesAround(unit.hexComponent, unit.movementLeft);
        HashSet<HexComponent> hexes = new HashSet<HexComponent>();
        //List<HexComponent> hexes = new List<HexComponent>();
        List<HexComponent> tmp = new List<HexComponent>();
        foreach (HexComponent hex in aroundHexes)
        {
            tmp = HexMovementPath(unit.hexComponent, hex, unit);
            if (tmp == null)
                continue;
            foreach (var hexComp in tmp)
            {
                hexes.Add(hexComp);
            }
        }
        // hexes = hs.;
        return hexes.ToList();
        //return hexes;
    }

    /// <summary>
    /// Returns a list with shared elements of both given lists
    /// 
    /// </summary>
    /// <param name="list1">First list of hexes</param>
    /// <param name="list2">Second list of hexes</param>
    /// <returns></returns>
    public static List<HexComponent> Intersection(List<HexComponent> list1, List<HexComponent> list2)
    {
        List<HexComponent> intersection = new List<HexComponent>();

        foreach (HexComponent el in list1)
        {
            if (list2.Contains(el))
            {
                intersection.Add(el);
            }
        }

        return intersection;
    }

    public static HexComponent GetEmptyHexInRange(HexComponent target, Unit unit)
    {
        List<HexComponent> availableHexes = GetMovementHexes(unit);
        List<HexComponent> hexesFromTarget = GetHexesAround(target, 1);
        List<HexComponent> intersection = Intersection(availableHexes, hexesFromTarget);

        if (intersection.Count == 0)
        {
            Debug.Log(unit.name + " couldn't get empty hex next to " + target);
            return null;
        }

        return intersection[Random.Range(0, intersection.Count)];
    }

    /// <summary>
    /// 
    /// Creates a path of hex tiles from one hex to another
    /// </summary>
    /// <param name="startingHex">The hex from which the path begins</param>
    /// <param name="destinyHex">The destination</param>
    /// <returns></returns>
    public static List<HexComponent> HexPath(HexComponent startingHex, HexComponent destinyHex)
    {
        List<HexComponent> hexPath = new List<HexComponent>();
        int distance = (int)Distance(startingHex.hex, destinyHex.hex);
        HexComponent closestHex = startingHex;

        for (int i = 0; i < distance; i++)
        {
            List<HexComponent> neighbours = GetHexesAround(closestHex, 1);

            if (neighbours.Count != 0)
            {
                closestHex = neighbours[0];
                int closestDistance = (int)Distance(closestHex.hex, destinyHex.hex);

                for (int j = 0; j < neighbours.Count; j++)
                {
                    if (closestDistance > (int)Distance(neighbours[j].hex, destinyHex.hex))
                    {
                        closestHex = neighbours[j];
                        closestDistance = (int)Distance(neighbours[j].hex, destinyHex.hex);
                    }
                }
            }

            hexPath.Add(closestHex);
        }

        return hexPath;
    }

    /// <summary>
    /// 
    /// Creates a path of hex tiles from one hex to another
    /// </summary>
    /// <param name="startingHex">The hex from which the path begins</param>
    /// <param name="destinyHex">The destination</param>
    /// <returns></returns>
    public static List<HexComponent> HexMovementPath(HexComponent startingHex, HexComponent destinyHex, Unit unit)
    {
        List<HexComponent> hexPath = new List<HexComponent>();
        int distance = (int)Distance(startingHex.hex, destinyHex.hex);
        //List<HexComponent> allowedHexes = GetMovementHexes(startingHex, distance, unit);
        HexComponent closestHex = startingHex;
        //Debug.Log(unit.name + " has " + unit.movementLeft.ToString() + " movement left!");
        for (int i = 0; i < unit.movementLeft; i++)
        {
            List<HexComponent> neighbours = GetNeighbourHexComponents(closestHex);
            //List<HexComponent> neighbours = GetHexesAround(closestHex, 1);
            neighbours = unit.FilterMovementHexes(neighbours);
            if (neighbours.Count != 0)
            {
                // closestHex = neighbours[0];
                int closestDistance = (int)Distance(closestHex.hex, destinyHex.hex);

                for (int j = 0; j < neighbours.Count; j++)
                {
                    if (closestDistance > (int)Distance(neighbours[j].hex, destinyHex.hex))
                    {
                        if (unit.CanMoveThrough(neighbours[j].hex))
                        {
                            closestHex = neighbours[j];
                            closestDistance = (int)Distance(neighbours[j].hex, destinyHex.hex);
                        }
                    }
                }
            }
            if (!hexPath.Contains(closestHex) && unit.CanMoveThrough(closestHex.hex))
            {
                hexPath.Add(closestHex);
            }
            else
            {
               // Debug.Log("Do hexpath contain currentClosest hex? : " + hexPath.Contains(closestHex) + "\n"
               //     + " Can unit move through? : " + unit.CanMoveThrough(closestHex.hex));
            }
        }
        //hexPath.Add(destinyHex);
        // if (hexPath.Count != distance)
       // if (hexPath.Count != unit.movementLeft)
       //     return null;
        //Debug.Log(unit.name + " HexPath has " + hexPath.Count.ToString() + " hexes in it!");
        return hexPath;
    }

    public static List<HexComponent> ShortestPathMovement(HexComponent startingHex, HexComponent destinyHex, Unit unit)
    {
        List<HexComponent> hexPath = new List<HexComponent>();
        // int distance;// = (int)Distance(startingHex.hex, destinyHex.hex);
        //if (distance > unit.movementLeft)
        int distance = unit.movementLeft;
        //List<HexComponent> allowedHexes = GetMovementHexes(startingHex, distance, unit);
        HexComponent closestHex = startingHex;

        for (int i = 0; i < distance; i++)
        {
            List<HexComponent> neighbours = GetNeighbourHexComponents(closestHex);
            //List<HexComponent> neighbours = GetHexesAround(closestHex, 1);
            if (neighbours.Count != 0)
            {
                closestHex = neighbours[0];
                int closestDistance = (int)Distance(closestHex.hex, destinyHex.hex);

                for (int j = 0; j < neighbours.Count; j++)
                {
                    if (closestDistance > (int)Distance(neighbours[j].hex, destinyHex.hex))
                    {
                        // if (unit.CanMoveThrough(neighbours[j].hex))
                        {
                            closestHex = neighbours[j];
                            closestDistance = (int)Distance(neighbours[j].hex, destinyHex.hex);
                        }
                    }
                }
            }
            if (unit.CanMoveThrough(closestHex.hex))
            {
                hexPath.Add(closestHex);
            }
        }
        //hexPath.Add(destinyHex);
        //if (hexPath.Count != distance)
        // return null;

        return hexPath;
    }

    public static List<HexComponent> GetNeighbourHexComponents(HexComponent hexComponent)
    {
        Map map = hexComponent.map;
        List<Hex> hexes = GetNeighbourHexes(hexComponent);
        List<HexComponent> neighbours = new List<HexComponent>();
        HexComponent neighbour;
        foreach (Hex hex in hexes)
        {
            neighbour = map.hexToHexComponentMap[hex];
            neighbours.Add(neighbour);
        }
        return neighbours;
    }

    public static List<Hex> GetNeighbourHexes(HexComponent hexComponent)
    {
        List<Hex> neighbours = new List<Hex>();
        Map map = hexComponent.map;
        Hex hex = hexComponent.hex;
        Hex tmp;

        tmp = (map.GetHexAt(hex.Q + 1, hex.R + 0));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            //Debug.LogError("Hex accessor tmp null");
            tmp = null;

        tmp = (map.GetHexAt(hex.Q + -1, hex.R + 0));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            //Debug.LogError("Hex accessor tmp null");
            tmp = null;

        tmp = (map.GetHexAt(hex.Q + 0, hex.R + +1));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            //Debug.LogError("Hex accessor tmp null");
            tmp = null;

        tmp = (map.GetHexAt(hex.Q + 0, hex.R + -1));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            // Debug.LogError("Hex accessor tmp null");
            tmp = null;


        tmp = (map.GetHexAt(hex.Q + 1, hex.R + -1));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            // Debug.LogError("Hex accessor tmp null");
            tmp = null;


        tmp = (map.GetHexAt(hex.Q + -1, hex.R + 1));
        if (tmp != null)
            neighbours.Add(tmp);
        else
            //Debug.LogError("Hex accessor tmp null");
            tmp = null;

        //because of cubic coordinates, very easy to guess which hexes are neighbours
        //neighbours.Add(map.GetHexAt(hex.Q + 1, hex.R + 0));
        //neighbours.Add(map.GetHexAt(hex.Q + -1, hex.R + 0));
        //neighbours.Add(map.GetHexAt(hex.Q + 0, hex.R + +1));
        //neighbours.Add(map.GetHexAt(hex.Q + 0, hex.R + -1));
        //neighbours.Add(map.GetHexAt(hex.Q + +1, hex.R + -1));
        //neighbours.Add(map.GetHexAt(hex.Q + -1, hex.R + +1));

        return neighbours;
    }

    public static List<HexComponent> CheckForObjects(List<HexComponent> hexes)
    {
        List<HexComponent> result = new List<HexComponent>();

        foreach (var hex in hexes)
        {
            if (hex.hex.objectOnHex == OBJECT_TYPE.None)
                result.Add(hex);
        }
        return result;
    }

    /// <summary>
    /// Checks, if provided hexes have feature on them from the list
    /// </summary>
    /// <param name="hexes">List of hexes to check</param>
    /// <param name="features">List of map features to check for</param>
    /// <returns>List of hexes that have feature</returns>
    public static List<HexComponent> CheckForFeatures(List<HexComponent> hexes, List<FEATURE_TYPE> features)
    {
        List<HexComponent> result = new List<HexComponent>();
        if (features.Count == 0)
            return null;
        int i = 0;
        foreach (var hex in hexes)
        {
            if (features.Contains(hex.hex.FeatureType))
                result.Add(hex);
            if (i < features.Count - 1)
                i++;
        }
        return result;
    }

}
