﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlaymodeEditor : MonoBehaviour
{
    public static PlaymodeEditor Instance;
    public Map map;
    public InputField mapNameInput;
    public InputField rowsNumberInput;
    public InputField columnNumberInput;

    public TERRAIN_TYPE selectedTerrainType;
    List<HexComponent> hexes = new List<HexComponent>();
    private HexComponent hexHit;
    private MapObject objectHit;

    public GameObject selectedObject;

    public bool isEditingTerrain = false;
    public bool isPlacingObjects = false;
    public Slider brushSlider;
    public int hexBrushSize = 1;
    public Text brushSizeMinText;
    public Text brushSizeMaxText;
    public Text brushSizeCurrentText;

    public Dropdown objectDropdown;
    public GameObject playerUnitsHolder;
    public GameObject enemyUnitsHolder;

    public MapObjectPrefabs objectPrefabs;
    private void Awake()
    {
        Instance = this;
        rowsNumberInput.contentType = InputField.ContentType.IntegerNumber;
        rowsNumberInput.characterValidation = InputField.CharacterValidation.Integer;
        columnNumberInput.contentType = InputField.ContentType.IntegerNumber;
        columnNumberInput.characterValidation = InputField.CharacterValidation.Integer;
        brushSizeMaxText.text = brushSlider.maxValue.ToString();
        brushSizeMinText.text = brushSlider.minValue.ToString();
        brushSlider.onValueChanged.AddListener(ChangeHexBrushSize);

        PopulateDropdown(objectDropdown, objectPrefabs.objects.ToArray());
    }


    private void Update()
    {


        if (Input.GetMouseButton(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (isEditingTerrain)
                {
                    EditAreaHex();
                }

                if (isPlacingObjects)
                {
                    PlaceObject();
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            isEditingTerrain = false;
            ClearObject();
        }

    }

    public void ClearObject()
    {
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("MapObject")))
        {
            if ((objectHit = rayHit.collider.GetComponent<MapObject>()) != null)
            {

                hexHit = objectHit.hexComponent;
                hexHit.mapObject = null;
                hexHit.hex.hasObject = false;
                hexHit.hex.FeatureType = FEATURE_TYPE.NONE;
                hexHit.hex.objectOnHex = OBJECT_TYPE.None;
                Destroy(objectHit.gameObject);


            }
        }
    }

    void PlaceObject()
    {
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("HexTile")))
        {
            if ((hexHit = rayHit.collider.GetComponent<HexComponent>()) != null)
            {
                if (hexHit.mapObject == null)
                {
                    MapObject placedObject = selectedObject.GetComponent<MapObject>();
                    if (placedObject == null)
                        return;
                    if (placedObject.factionID == MapObject.FACTION_ID.PLAYER)
                        Instantiate(selectedObject, hexHit.transform.position, hexHit.transform.rotation, playerUnitsHolder.transform);
                    else
                    {
                        Instantiate(selectedObject, hexHit.transform.position, hexHit.transform.rotation, enemyUnitsHolder.transform);
                    }

                    hexHit.mapObject = placedObject;
                    placedObject.hexComponent = hexHit;
                    placedObject.hex = hexHit.hex;
                    placedObject.hex.objectOnHex = placedObject.objectType;

                    //need to make it separate later
                    if (placedObject is MapFeature)
                    {
                        hexHit.hex.FeatureType = placedObject.GetComponent<MapFeature>().featureType;
                    }

                }

            }
        }
    }

    void EditAreaHex()
    {
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("HexTile")))
        {
            if ((hexHit = rayHit.collider.GetComponent<HexComponent>()) != null)
            {
                hexes = HexAccessor.GetHexesAround(hexHit, hexBrushSize);

                if (hexes == null)
                    return;

                foreach (var hex in hexes)
                {
                    hex.hex.TerrainType = selectedTerrainType;
                    hex.UpdateTerrain();
                    hex.map.UpdateSingleHexVisual(hex);
                }
            }
        }
    }

    public void LoadMap()
    {
        map.LoadMap(mapNameInput.text);
    }

    public void SaveMap()
    {
        map.mapData.mapName = mapNameInput.text;
        map.mapName = mapNameInput.text;
        SaveSystem.SaveMap(map.mapData);
    }

    public void CreateNewMap()
    {
        map.rows = int.Parse(rowsNumberInput.text);
        map.columns = int.Parse(columnNumberInput.text);
        map.mapName = mapNameInput.text;
        map.CreateNewMap();
    }

    public void SelectTerrain(int option)
    {
        isEditingTerrain = true;
        isPlacingObjects = false;
        switch (option)
        {
            case 0:
                selectedTerrainType = TERRAIN_TYPE.WATER;
                break;
            case 1:
                selectedTerrainType = TERRAIN_TYPE.EARTH;
                break;
            case 2:
                selectedTerrainType = TERRAIN_TYPE.GRASS;
                break;

        }
    }

    public void SelectObject(int option)
    {
        isEditingTerrain = false;
        isPlacingObjects = true;
        selectedObject = objectPrefabs.objects[option];
    }

    public void ChangeHexBrushSize(float size)
    {
        hexBrushSize = (int)size;
        brushSizeCurrentText.text = hexBrushSize.ToString();
    }

    public void PopulateDropdown(Dropdown dropdown, GameObject[] optionsArray)
    {
        List<string> options = new List<string>();
        foreach (var option in optionsArray)
        {
            options.Add(option.name);
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(options);
    }
}
