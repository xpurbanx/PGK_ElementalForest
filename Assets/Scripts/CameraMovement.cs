﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// moves the camera
public class CameraMovement : MonoBehaviour
{
    private float moveSpeed = 0.2f;
    private float rotationSpeed = 0.2f;
    private float zoomSpeed = 0.2f;

    public float northLimit = 10f;
    public float southLimit = -5f;
    public float westLimit = 2f;
    public float eastLimit = 15f;

    public float zoomUpLimit = 10f;
    public float zoomDownLimit = 2f;

    //private float yaw = 00.0f;
    //private float pitch = 50.0f;

    private Vector3 lastMouse = new Vector3(255, 255, 255);

    private float xPosition;
    private float yPosition;
    private void Update()
    {
        // moves the camera in the four cardinal directions
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            float posY = transform.localPosition.y;
            transform.position += moveSpeed * Input.GetAxisRaw("Vertical") * transform.forward;
            transform.position += moveSpeed * Input.GetAxisRaw("Horizontal") * transform.right;
            transform.position = new Vector3(transform.position.x, posY, transform.position.z);
            checkPositionLimits();
        }

        // rotates the camera
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.E))
        {
            int a = Input.GetKey(KeyCode.Q) ? 1 : 0;
            int b = Input.GetKey(KeyCode.E) ? 1 : 0;
            transform.Rotate(0, rotationSpeed * (b - a), 0, Space.World);
        }

        if (Input.GetMouseButton(2))
        {
            lastMouse = Input.mousePosition - lastMouse;
            lastMouse = new Vector3(-lastMouse.y * rotationSpeed, lastMouse.x * rotationSpeed, 0);
            lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
            transform.eulerAngles = lastMouse;

        }
        lastMouse = Input.mousePosition;

        //if (Input.GetMouseButtonUp(2))
        //{

        //}

        //if (Input.GetMouseButton(2))
        //{
        //    yaw -= rotationSpeed * (Input.GetAxis("Mouse X") - xPosition);
        //    pitch += rotationSpeed * (Input.GetAxis("Mouse Y") - yPosition);

        //    transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        //}

        // zooms the camera in and out
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X))
        {
            int a = Input.GetKey(KeyCode.Z) ? 1 : 0;
            int b = Input.GetKey(KeyCode.X) ? 1 : 0;
            float posX = transform.localPosition.x;
            float posZ = transform.localPosition.z;
            transform.localPosition += zoomSpeed * (a - b) * transform.forward;
            if (checkZoomLimits())
            {
                transform.position = new Vector3(posX, transform.position.y, posZ);
            }
            checkPositionLimits();
        }

        //blocks clicking through UI
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            // MMB zoom
            if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
            {
                float a = Input.GetAxis("Mouse ScrollWheel");
                //Debug.Log(a.ToString());
                float posX = transform.localPosition.x;
                float posZ = transform.localPosition.z;
                transform.localPosition += 10 * zoomSpeed * a * transform.forward;
                if (checkZoomLimits())
                {
                    transform.position = new Vector3(posX, transform.position.y, posZ);
                }
                checkPositionLimits();
            }
        }
    }

    private void checkPositionLimits()
    {

        if (transform.position.x > eastLimit)
            transform.position = new Vector3(eastLimit, transform.position.y, transform.position.z);
        else if (transform.position.x < westLimit)
            transform.position = new Vector3(westLimit, transform.position.y, transform.position.z);

        if (transform.position.z > northLimit)
            transform.position = new Vector3(transform.position.x, transform.position.y, northLimit);
        else if (transform.position.z < southLimit)
            transform.position = new Vector3(transform.position.x, transform.position.y, southLimit);
    }

    private bool checkZoomLimits()
    {
        bool limit = false;


        if (transform.position.y > zoomUpLimit)
        {
            transform.position = new Vector3(transform.position.x, zoomUpLimit, transform.position.z);
            limit = true;
        }
        else if (transform.position.y < zoomDownLimit)
        {
            transform.position = new Vector3(transform.position.x, zoomDownLimit, transform.position.z);
            limit = true;
        }

        return limit;
    }

}
