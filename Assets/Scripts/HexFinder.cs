﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Just for finding hex at start
public class HexFinder : MonoBehaviour
{
    MapObject parent;
    public HexComponent currentHex;
    private void Awake()
    {
        parent = GetComponentInParent<MapObject>();
    }
    private void Start()
    {

        Invoke("VerifyHexComponent", 0.1f);
    }

    public void VerifyHexComponent()
    {
        if (currentHex == null)
        {
            Debug.Log(parent.name + " is veryfying hex and its null!");
            return;
        }
        if (currentHex.mapObject != parent)
        {
            currentHex.ClearObjectInfo(parent);
            currentHex.AddObjectInfo(parent);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        GameObject otherObj = collider.gameObject;
        //Debug.Log("Collided with: " + otherObj);
        if (collider.tag == "HexTile")
        {
            currentHex = collider.GetComponent<HexComponent>();
            if (parent.hexComponent == null)
            {
                parent.hexComponent = currentHex;
                parent.hexComponent.hex = currentHex.hex;
                //currentHex.mapObject = parent;
                currentHex.AddObjectInfo(parent);
            }

        }
    }

}
