﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public UIManager uIManager;
    public Click click;
    public Map map;
    public TurnManager turnManager;

    private IEnumerator targetSelection;
    private Coroutine currentargetSelection;

    public bool isGameActive = true;

    private void Awake()
    {
        Instance = this;
        click = FindObjectOfType<Click>();
        click.gameManager = this;
        if (click.enabled == false)
            click.enabled = true;
        //targetSelection = TargetSelect();
    }
    private void OnEnable()
    {
        
    }
    private void Start()
    {
        GameStart();
    }


    public void GameStart()
    {
        turnManager.StartTurnChecking();
        UnitManager.Instance.currentForestValue = UnitManager.Instance.startForestValue;
    }

    public void TargetObjectSelect(MapObject callback)
    {
        StopAllCoroutines();
        uIManager.promptText.text = "Select enemy unit as target!";
        click.RemoveTarget();
        click.ChangeClickMode(Click.CLICK_MODE.TargetObject);
        StartCoroutine(WaitForNewTargetObject(callback));
    }

    public void TargetHexSelect(HexComponent callback)
    {
        StopAllCoroutines();
        uIManager.promptText.text = "Select hex as target!";
        click.RemoveTarget();
        click.ChangeClickMode(Click.CLICK_MODE.TargetHex);
        StartCoroutine(WaitForNewTargetHex(callback));
    }

    public IEnumerator WaitForNewTargetObject(MapObject callback)
    {
        click.RemoveTarget();
        while (click.targetedObject == null)
        {
            //Debug.Log("Waiting for target object!");
            yield return new WaitForSeconds(0.3f);
        }
        Debug.Log("Have Target object! Target: " + click.targetedObject.name);
        callback = click.targetedObject;
        click.RemoveTarget();
    }

    public IEnumerator WaitForNewTargetHex(HexComponent callback)
    {
        click.RemoveTarget();
        while (click.targetedHex == null)
        {
            Debug.Log("Waiting for target hex!");
            yield return new WaitForSeconds(0.3f);
        }
        Debug.Log("Have Target hex! Target: " + click.targetedHex.name);
        callback = click.targetedHex;
        click.RemoveTarget();
    }



}
