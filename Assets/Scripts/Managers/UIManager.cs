﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using TMPro;
public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public GameManager gameManager;
    public GameObject generalUI;
    public Button fuseButton;
    public Button deathButton;
    public GameObject endTurnButton;
    //public Text turnTypeText;
    public Text promptText;
    public GameObject tutorialWindow;
    public GameObject unitUI;
    public GameObject unitAbilityButtons;
    public TextMeshProUGUI unitName;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI manaText;
    public TextMeshProUGUI movemenLeft;
    public TextMeshProUGUI canActText;
    public Image actionLeftIcon;
    public Image actionUsedIcon;
    public List<Button> abilityButtons;
    //public List<GameObject> abilityDescriptions;
    public List<TextMeshProUGUI> abilityHealthCostTexts;
    public GameObject abilityDescription;
    public Text abilityDescriptionText;

    public Text elementalText;
    public Text treantText;
    public Text enemiesText;
    public Text forestText;
    public Text turnText;

    public Sprite hexSprite;
    public Sprite objectSprite;
    public Sprite selfSprite;
    public Sprite errorSprite;

    public GameObject subInfo;
    public Text hasSeedText;

    public GameObject objectiveSmall;
    public GameObject objectiveLarge;
    public GameObject objectiveName;
    public GameObject objectiveDescription;

    public GameObject tutorialSlider;

    public Text battleLogText;

    public GameObject mouseOverUI;
    public GameObject mouseOverPanel;
    public Text unitMouseOverNameText;
    public Text unitMouseOverHPText;

    public GameObject pauseUI;
    public GameObject musicSlider;
    public GameObject soundSlider;
    public GameObject victoryScreen;
    public Text textMessage;

    private void Awake()
    {

        Instance = this;
        unitUI.SetActive(false);
        foreach (Button button in unitAbilityButtons.GetComponentsInChildren<Button>())
        {
            if (!abilityButtons.Contains(button))
            {
                abilityButtons.Add(button);
            }
        }

        forestText.text = "Deforestation: " + "0" + "%";// temporary shows deforestation level at 0%
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            generalUI.SetActive(false);
            unitUI.SetActive(false);
            mouseOverUI.SetActive(false);
            pauseUI.SetActive(true);
        }
        if (abilityDescription.gameObject.activeSelf == true)
        {
            UpdateHighlightPosition();
        }

        Hotkeys();
    }

    public void ShowVictoryScreen()
    {
        generalUI.SetActive(false);
        unitUI.SetActive(false);
        mouseOverUI.SetActive(false);
        victoryScreen.SetActive(true);
    }

    public void PauseMenuOn()
    {
        generalUI.SetActive(false);
        unitUI.SetActive(false);
        mouseOverUI.SetActive(false);
        pauseUI.SetActive(true);
    }

    public void UpdateSliders()
    {
        musicSlider.GetComponent<Slider>().value = AudioManager.Instance.GetMusicVolume();
        soundSlider.GetComponent<Slider>().value = AudioManager.Instance.GetSoundVolume();
    }

    public void PauseMenuOff()
    {
        generalUI.SetActive(true);
        //unitUI.SetActive(false);
        mouseOverUI.SetActive(true);
        pauseUI.SetActive(false);
    }

    public void ToggleTutorialDescription()
    {
        if (tutorialSlider.activeSelf)
            tutorialSlider.SetActive(false);
        else
            tutorialSlider.SetActive(true);
    }

    public void ShowObjectiveDescription()
    {
        objectiveSmall.SetActive(false);
        objectiveLarge.SetActive(true);
    }

    public void HideObjectiveDescription()
    {
        objectiveSmall.SetActive(true);
        objectiveLarge.SetActive(false);
    }

    public void SetObjectiveName(string s)
    {
        objectiveName.GetComponent<Text>().text = s;
    }

    public void SetObjectiveDescription(string s)
    {
        objectiveDescription.GetComponent<Text>().text = s;
    }

    public void AddBattleLog(string s)
    {
        battleLogText.text = string.Concat(battleLogText.text, '\n', s);
        if (battleLogText.text.Length > 900)
        {
            int pos = battleLogText.text.Length - 900;
            battleLogText.text = battleLogText.text.Substring(pos);
        }
    }

    public void ClearPrompt()
    {
        promptText.text = "";
    }

    public void UnitUIOn()
    {
        unitUI.SetActive(true);
    }

    public void UnitUIOff()
    {
        unitUI.SetActive(false);
    }

    public void SubInfoOn()
    {
        subInfo.SetActive(true);
    }

    public void SubInfoOff()
    {
        subInfo.SetActive(false);
    }

    public void ToogleTutorialWindow()
    {
        if (tutorialWindow != null)
            tutorialWindow.SetActive(!tutorialWindow.activeSelf);
    }

    public void UpdateUnitMouseOver(MapObject mapObject)
    {
        if (mapObject == null)
        {
            mouseOverPanel.gameObject.SetActive(false);
            unitMouseOverNameText.text = "";
            unitMouseOverHPText.text = "";
            if (DebugUI.Instance != null)
            {
                DebugUI.Instance.debugMouseover.SetActive(false);
            }
        }
        else
        {
            mouseOverPanel.gameObject.SetActive(true);
            unitMouseOverNameText.text = "Unit name: " + mapObject.nameObject;
            unitMouseOverHPText.text = "Unit health: " + mapObject.currentHealth + "/" + mapObject.healthMax;

            if (DebugUI.Instance != null)
            {
                DebugUI.Instance.debugMouseover.SetActive(true);
                DebugUI.Instance.mapObjectIDText.text = mapObject.ID.ToString();
            }
        }
    }

    public void UpdateUnitUIInfo(Unit unit)
    {
        if (unit.factionID == MapObject.FACTION_ID.PLAYER)
        {
            UnitUIOn();
            HideHighlightedAbility();
            unitName.text = unit.nameObject;
            movemenLeft.text = +unit.movementLeft + "/" + unit.movementBase;
            healthText.text = unit.currentHealth + "/" + unit.healthMax;
            if (unit.manaUser)
                manaText.text = unit.manaCurrent + "/" + unit.manaMax;
            else
                manaText.text = "";
            //canActText.text = "Can act this turn: " + !unit.actionDone;
            //descriptionText.text = unit.description;
            if (!unit.actionDone)
            {
                actionLeftIcon.gameObject.SetActive(true);
                actionUsedIcon.gameObject.SetActive(false);
            }
            else
            {
                actionLeftIcon.gameObject.SetActive(false);
                actionUsedIcon.gameObject.SetActive(true);
            }
            UpdateUnitAbilitiesUI(unit);
            subInfo.SetActive(false);
            if (unit is Elemental)
            {
                Elemental elemental = unit.GetComponent<Elemental>();
                if (elemental.elementalType == ELEMENTAL_TYPE.Air)
                {
                    subInfo.SetActive(true);
                    //if (elemental.GetComponentInChildren<ASeedPickup>().seedPicked == true)
                    if (elemental.GetComponentInChildren<ASeedporter>().seedPicked == true)
                    {
                        hasSeedText.text = "Has seed: yes";
                    }
                    else
                    {
                        hasSeedText.text = "Has seed: no";
                    }
                }
            }

        }

    }

    public void UpdateUnitAbilitiesUI(Unit unit)
    {
        if (unit.factionID == MapObject.FACTION_ID.PLAYER)
        {
            TooltipTrigger tooltip;



            if (unit.abilityList.abilities.Count - 2 > abilityButtons.Count)
            {
                Debug.LogError("Unit has more abilities than buttons in UI!");
            }
            int i = 0;



            deathButton.onClick.RemoveAllListeners();
            fuseButton.onClick.RemoveAllListeners();
            fuseButton.gameObject.SetActive(false);
            deathButton.gameObject.SetActive(false);

            foreach (Ability ability in unit.abilityList.abilities)
            {
                if (IsAbilityDisabled(ability))
                {
                    continue;
                }
                if (i < abilityButtons.Count)
                {
                    abilityButtons[i].onClick.RemoveAllListeners();

                }
                if (ability.isPassive == false)
                {
                    EventTrigger.Entry entryEvent = new EventTrigger.Entry();
                    entryEvent.eventID = EventTriggerType.PointerEnter;

                    EventTrigger.Entry exitEvent = new EventTrigger.Entry();
                    exitEvent.eventID = EventTriggerType.PointerExit;

                    if (ability is ASelfDestroy)
                    {
                        deathButton.gameObject.SetActive(true);
                        if (unit.CanAct() && ability.CanBeUsed() && !ability.isPassive)
                        {
                            deathButton.enabled = true;
                        }
                        else
                        {
                            deathButton.enabled = false;
                        }
                        deathButton.onClick.AddListener(delegate { ability.Use(); });
                        // entryEvent.callback.AddListener((eventData) => { ShowHighligtedAbility(unit, ability.description); });
                        //exitEvent.callback.AddListener((eventData) => { HideHighlightedAbility(); });
                        deathButton.gameObject.AddComponent<EventTrigger>();
                        deathButton.gameObject.GetComponent<EventTrigger>().triggers.Add(entryEvent);
                        deathButton.gameObject.GetComponent<EventTrigger>().triggers.Add(exitEvent);
                    }
                    else if (ability is AFuse)
                    {
                        fuseButton.gameObject.SetActive(true);
                        if (unit.CanAct() && ability.CanBeUsed() && !ability.isPassive)
                        {
                            fuseButton.enabled = true;
                        }
                        else
                        {
                            fuseButton.enabled = false;
                        }
                        tooltip = fuseButton.GetComponent<TooltipTrigger>();
                        fuseButton.onClick.AddListener(delegate { ability.Use(); });
                        entryEvent.callback.AddListener((eventData) => { ShowHighligtedAbility(unit, ability.description, tooltip); });
                        exitEvent.callback.AddListener((eventData) => { HideHighlightedAbility(tooltip); });
                        fuseButton.gameObject.AddComponent<EventTrigger>();
                        fuseButton.gameObject.GetComponent<EventTrigger>().triggers.Add(entryEvent);
                        fuseButton.gameObject.GetComponent<EventTrigger>().triggers.Add(exitEvent);
                    }
                    else
                    {
                        abilityButtons[i].gameObject.SetActive(true);
                        tooltip = abilityButtons[i].GetComponent<TooltipTrigger>();
                        string cause = "";
                        if (unit.CanAct() && ability.CanBeUsed() && !ability.isPassive)
                        {
                            abilityButtons[i].enabled = true;
                        }
                        else
                        {
                            abilityButtons[i].enabled = false;
                            if (!unit.CanAct())
                            {
                                cause = "Unit has no action left";
                            }
                            else if (!ability.CanBeUsed())
                            {
                                cause = ability.FindCause();
                            }
                        }

                        if (ability.healthCost != 0)
                        {
                            abilityButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = ability.healthCost.ToString();
                            if (unit.manaUser)
                            {
                                abilityButtons[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.blue;
                            }
                            else
                                abilityButtons[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
                        }
                        else
                            abilityButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = "";


                        abilityButtons[i].GetComponentInChildren<Text>().text = ability.abilityName;
                        abilityButtons[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = AbilitySprite(ability.targetType);
                        abilityButtons[i].onClick.AddListener(delegate { ability.Use(); });

                        entryEvent.callback.AddListener((eventData) => { ShowHighligtedAbility(unit, ability.description, tooltip, cause); });
                        exitEvent.callback.AddListener((eventData) => { HideHighlightedAbility(tooltip); });

                        abilityButtons[i].gameObject.AddComponent<EventTrigger>();
                        abilityButtons[i].gameObject.GetComponent<EventTrigger>().triggers.Add(entryEvent);
                        abilityButtons[i].gameObject.GetComponent<EventTrigger>().triggers.Add(exitEvent);
                        i++;
                    }
                }
                else
                {
                    abilityButtons[i].gameObject.SetActive(false);
                }
                //abilityDescriptions[i].gameObject.SetActive(true);
                //abilityDescriptionsTexts[i].text = ability.description;
            }
            int specialAbilities = 0;
            if (fuseButton != null)
            {
                specialAbilities++;
            }
            if (deathButton != null)
            {
                specialAbilities++;
            }
            if (unit.abilityList.abilities.Count < abilityButtons.Count + specialAbilities)
            {
                for (; i <= abilityButtons.Count - 1; i++)
                {
                    abilityButtons[i].gameObject.SetActive(false);
                    //abilityDescriptions[i].gameObject.SetActive(false);
                }
            }
            /*
            if (unit.abilityList.abilities.Count < abilityButtons.Count)
            {
                for (; i <= abilityButtons.Count - 1; i++)
                {
                    abilityButtons[i].gameObject.SetActive(false);
                    abilityDescriptions[i].gameObject.SetActive(false);
                }
            }  
            */
        }

    }

    public void ShowHighligtedAbility(Unit unit, string desc, TooltipTrigger tooltip, string cause = "")
    {
        abilityDescription.gameObject.SetActive(true);
        abilityDescriptionText.text = desc + "\n\n" + cause;
        //tooltip.content = desc + "\n\n" + cause;
        abilityDescription.gameObject.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

    }

    public void UpdateHighlightPosition()
    {
        abilityDescription.gameObject.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
    }

    public void HideHighlightedAbility()
    {
        abilityDescription.gameObject.SetActive(false);
        abilityDescriptionText.text = "";
        //tooltip.content = "";
        //tooltip.header = "";
    }
    public void HideHighlightedAbility(TooltipTrigger tooltip)
    {
        abilityDescription.gameObject.SetActive(false);
        abilityDescriptionText.text = "";
        //tooltip.content = "";
        //tooltip.header = "";
    }

    public bool IsAbilityDisabled(Ability ability)
    {
        foreach (Ability tmp in AbilityManager.Instance.disabledAbilities)
        {
            if (ability.GetType() == tmp.GetType())
            {
                //Debug.Log("Ability type: " + ability.GetType() + "         Disabled Type: " + tmp.GetType());
                return true;
            }
        }

        //if (AbilityManager.Instance.disabledAbilities.Any(x => x.(AbilityManager.Instance.disabledAbilities.GetType()))
        //{
        //    return true;
        //}

        return false;
    }

    private Sprite AbilitySprite(ABILITY_TARGET target)
    {
        switch (target)
        {
            case ABILITY_TARGET.HEX:
                return hexSprite;
            case ABILITY_TARGET.OBJECT:
                return objectSprite;
            case ABILITY_TARGET.SELF:
                return selfSprite;
            default:
                Debug.Log("No ability icon");
                return errorSprite;
        }
    }

    void Hotkeys()
    {
        if (unitUI.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (abilityButtons[0].gameObject.activeSelf)
                {
                    abilityButtons[0].onClick.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (abilityButtons[1].gameObject.activeSelf)
                {
                    abilityButtons[1].onClick.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (abilityButtons[2].gameObject.activeSelf)
                {
                    abilityButtons[2].onClick.Invoke();
                }
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (abilityButtons[3].gameObject.activeSelf)
                {
                    abilityButtons[3].onClick.Invoke();
                }
            }

        }



        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (generalUI.gameObject.activeSelf && endTurnButton.gameObject.activeSelf && gameManager.turnManager.turnType == TurnManager.TurnType.Player)
            {
                endTurnButton.GetComponent<Button>().onClick.Invoke();
            }
        }


    }

}
