﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityManager : MonoBehaviour
{
    public static AbilityManager Instance;

    //public bool fireDisabled = false;
    //public bool waterDisabled = false;
    //public bool earthDisabled = false;
    //public bool airDisabled = false;
    //public bool throwRockDisabled = false;
    //public bool zeusStrikeDisabled = false;
    //public bool fireDisabled = false;

    public List<Ability> disabledAbilities = new List<Ability>();

    private void Awake()
    {
        Instance = this;
        LoadDisabledAbilities();
    }
    void LoadDisabledAbilities()
    {
        foreach (Ability ability in GetComponentsInChildren<Ability>())
        {
            if (!disabledAbilities.Contains(ability))
            {
                disabledAbilities.Add(ability);
            }
        }
    }



}
