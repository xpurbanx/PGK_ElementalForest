﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class UnitManager : MonoBehaviour
{
    public static UnitManager Instance;
    public List<Unit> playerUnits;

    public int elementalLimit = 12;
    public int treantLimit = 2;
    public int elementalCount;
    public int treantCount;
    public List<Unit> enemyUnits;
    public List<MapFeature> trees;
    public List<MapFeature> nonTrees;

    public bool mainTree = true;
    public int startForestValue;
    public int currentForestValue;
    [Range(0, 100)]
    float allowedDeforestation;
    float deforestation;
    public int IDCounter = 0;


    public MainTree mainTreeObject;
    private void Awake()
    {
        Instance = this;
        mainTree = true;
        playerUnits = new List<Unit>();
        enemyUnits = new List<Unit>();
        trees = new List<MapFeature>();
        nonTrees = new List<MapFeature>();
        elementalCount = 0;
        treantCount = 0;
        startForestValue = 0;
        deforestation = 0.0f;
        currentForestValue = 0;
    }

    private void Start()
    {
        StartCoroutine(LateStart(0.5f));
    }
    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        AssignIDsAtStart();
        CalculateStartForestation();

    }

    void AssignIDsAtStart()
    {
        foreach (MapObject playerObject in playerUnits)
        {
            if(playerObject.ID == 0)
            {
                playerObject.ID = GetNewID();
                playerObject.gameObject.name = playerObject.nameObject + " " + playerObject.ID.ToString();
            }
        }

        foreach (MapObject enemyObject in enemyUnits)
        {
            if (enemyObject.ID == 0)
            {
                enemyObject.ID = GetNewID();
                enemyObject.gameObject.name = enemyObject.nameObject + " " + enemyObject.ID.ToString();

            }
        }

        foreach (MapObject tree in trees)
        {
            if (tree.ID == 0)
            {
                tree.ID = GetNewID();
                tree.gameObject.name = tree.nameObject + " " + tree.ID.ToString();

            }
        }

        foreach (MapObject nonTree in nonTrees)
        {
            if (nonTree.ID == 0)
            {
                nonTree.ID = GetNewID();
                nonTree.gameObject.name = nonTree.nameObject + " " + nonTree.ID.ToString();
            }
        }
    }
    public int GetNewID()
    {
        IDCounter++;
        return IDCounter;
    }
    public void RemovePlayerUnit(Unit unit)
    {
        if (playerUnits.Contains(unit))
        {
            if (unit.objectType == OBJECT_TYPE.Treant)
                treantCount--;
            else if (unit is Elemental || unit is Seed)
                elementalCount--;

            playerUnits.Remove(unit);

            if (UIManager.Instance.elementalText != null && mainTreeObject != null)
            {
                UIManager.Instance.elementalText.text = "Elementals: " + elementalCount + "/" + elementalLimit.ToString();
                UIManager.Instance.treantText.text = "Treants: " + treantCount +"/" + treantLimit.ToString();
            }
        }
    }

    public void RemoveEnemyUnit(Unit unit)
    {
        if (enemyUnits.Contains(unit))
        {
            enemyUnits.Remove(unit);

            if (UIManager.Instance.elementalText != null)
                UIManager.Instance.enemiesText.text = "Enemies left: " + enemyUnits.Count.ToString();
        }
    }

    public void AddPlayerUnit(Unit unit)
    {
        if (!playerUnits.Contains(unit))
        {
            playerUnits.Add(unit);

            if (unit.objectType == OBJECT_TYPE.Treant)
                treantCount++;
            else if (unit is Elemental || unit is Seed)
                elementalCount++;

            if(unit is MainTree)
            {
                mainTreeObject = (MainTree)unit;
            }

            if (UIManager.Instance.elementalText != null && mainTreeObject != null)
            {
                UIManager.Instance.elementalText.text = "Elementals: " + elementalCount + "/" + elementalLimit.ToString();
                UIManager.Instance.treantText.text = "Treants: " + treantCount + "/" + treantLimit.ToString();
            }


        }
    }

    public void AddEnemyUnit(Unit unit)
    {
        if (!enemyUnits.Contains(unit))
        {
            enemyUnits.Add(unit);

            if (UIManager.Instance.elementalText != null)
                UIManager.Instance.enemiesText.text = "Enemies left: " + enemyUnits.Count.ToString();
        }
    }
    public void AddMapFeature(MapFeature mapFeature)
    {
        if (!nonTrees.Contains(mapFeature))
        {
            nonTrees.Add(mapFeature);
        }
    }
    public void AddTree(MapFeature mapFeature)
    {
        if (!trees.Contains(mapFeature))
        {
            trees.Add(mapFeature);

            if (UIManager.Instance.forestText != null && trees[0] != null)
            {
                //CalculateDeforestation();
                UIManager.Instance.forestText.text = "Deforestation: " + deforestation + "% / " + allowedDeforestation.ToString() +"%";
            }
        }
    }

    public void RemoveTree(MapFeature tree)
    {
        if (trees.Contains(tree))
        {
            trees.Remove(tree);

            if (tree.featureType == FEATURE_TYPE.LIGHTFOREST)
                currentForestValue -= 1;
            if (tree.featureType == FEATURE_TYPE.FOREST)
                currentForestValue -= 1;

            if (UIManager.Instance.forestText != null && trees[0] != null)
            {
                CalculateDeforestation();
                UIManager.Instance.forestText.text = "Deforestation: " + deforestation + "% / " + allowedDeforestation.ToString() + "%";
            }
        }
    }

    public void CalculateStartForestation()
    {
        startForestValue = 0;
        foreach (MapFeature tree in trees)
        {
            if (tree.featureType == FEATURE_TYPE.LIGHTFOREST)
                startForestValue += 1;
            if (tree.featureType == FEATURE_TYPE.FOREST)
                startForestValue += 2;
        }

        currentForestValue = startForestValue;
        allowedDeforestation = (1 - VictoryManager.Instance.deforestationThreshold) * 100;
    }

    public void CalculateDeforestation()
    {
        float tmp;
        tmp = 100 - ((currentForestValue * 100) / startForestValue);
        deforestation = tmp;
    }


}
