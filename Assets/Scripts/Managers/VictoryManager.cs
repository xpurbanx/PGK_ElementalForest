﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class VictoryManager : MonoBehaviour
{
    public static VictoryManager Instance;
    public Objective[] objectives;
    [HideInInspector]
    public Objective currentObjective = null;
    int index;

    public float deforestationThreshold = 0.5f;
    int turnLimiter;
    bool isTurnForVictory;


    public bool needRequiredObject;
    public bool needMinimalElementalLimit;
    public int minimalElementalLimit;
    public OBJECT_TYPE requiredObject;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        objectives = GetComponents<Objective>();
        
        if(objectives.Length > 0)
        {
            Array.Sort(objectives, delegate (Objective x, Objective y) { return x.order.CompareTo(y.order); });
            index = 0;
            currentObjective = objectives[index];
            currentObjective.UpdateDescription();
            UIManager.Instance.SetObjectiveName(currentObjective.name);
            UIManager.Instance.SetObjectiveDescription(currentObjective.description);
        }
            
        //string name = "Kill all goblins";
        //UIManager.Instance.SetObjectiveName(name);

    }

    private void Update()
    {
        VictoryConditions();
        
        if (UIManager.Instance.objectiveLarge.activeSelf)
        {
            currentObjective.UpdateDescription();
            UIManager.Instance.SetObjectiveName(currentObjective.name);
            UIManager.Instance.SetObjectiveDescription(currentObjective.description);
            //string desc = string.Concat("Kill the remaining ", UnitManager.Instance.enemyUnits.Count.ToString(), " goblins to win");
            //UIManager.Instance.SetObjectivfeDescription(desc);
        }
        
    }

    public void VictoryConditions()
    {
        //if (WinningConditions())
        //{
        //    UIManager.Instance.textMessage.text = "YOU WON!";
        //    UIManager.Instance.ShowVictoryScreen();
        //}

        if (currentObjective.SuccessConditions())
        {
            index++;
            if(objectives.Length <= index)
            {
                UIManager.Instance.textMessage.text = "YOU WON!";
                UIManager.Instance.ShowVictoryScreen();
            }
            else 
            {
                currentObjective = objectives[index];
                currentObjective.UpdateDescription();
                UIManager.Instance.SetObjectiveName(currentObjective.name);
                UIManager.Instance.SetObjectiveDescription(currentObjective.description);
            }
        }

        if (LosingConditions() || currentObjective.FailConditions())
        {
            UIManager.Instance.textMessage.text = "YOU LOST";
            UIManager.Instance.ShowVictoryScreen();
        }
    }

    //private bool WinningConditions()
    //{
    //    if (UnitManager.Instance.enemyUnits.Count == 0 && needRequiredObject == false)
    //    {
    //        Debug.Log("Enemies killed!");
    //        return true;
    //    }

    //    if (TurnManager.Instance.turnCounter == turnLimiter && isTurnForVictory)
    //    {
    //        Debug.Log("Turn timer!");
    //        return true;
    //    }

    //    if (UnitManager.Instance.enemyUnits.Count == 0 && needRequiredObject && CheckForRequiredObject())
    //    {
    //        Debug.Log("Has fire elemental?");
    //        return true;

    //    }

    //    return false;
    //}

    private bool LosingConditions()
    {
        if (!UnitManager.Instance.mainTree)
            return true;

        if (UnitManager.Instance.currentForestValue < deforestationThreshold * UnitManager.Instance.startForestValue)
            return true;

        if (TurnManager.Instance.turnCounter == turnLimiter && !isTurnForVictory)
            return true;

        if (needMinimalElementalLimit && MinElementalCountReached())
            return true;

        return false;
    }

    bool CheckForRequiredObject()
    {
        foreach (var unit in UnitManager.Instance.playerUnits)
        {
            if (unit.objectType == requiredObject)
            {
                return true;
            }
        }
        return false;
    }

    bool MinElementalCountReached()
    {
        if (UnitManager.Instance.elementalLimit <= minimalElementalLimit)
            return true;
        else
            return false;
    }
}
