﻿using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public GameObject musicSlider;
    public GameObject soundSlider;

    public bool soundEnabled;
    public bool musicEnabled;
    public float musicVolume;
    public float soundVolume;
    [HideInInspector]
    public AudioSource[] sources;

    private void Awake()
    {
        Instance = this;
        sources = GetComponentsInChildren<AudioSource>();
        musicEnabled = true;
        soundEnabled = true;
        if(sources[0].clip != null)
        {
            sources[0].loop = true;
            sources[0].Play();
        }

        musicSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate {
            SetMusicVolume(musicSlider.GetComponent<Slider>().value);
        });

        soundSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate {
            SetSoundVolume(soundSlider.GetComponent<Slider>().value);
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            soundEnabled = !soundEnabled;
            musicEnabled = !musicEnabled;
            SwitchMusicState(musicEnabled);
        }

    }

    public void LoadSound(AudioClip clip)
    {
        if(clip != null)
            sources[1].clip = clip;
    }

    public void PlaySound()
    {
        if(soundEnabled && sources[1].clip != null)
        {
            sources[1].Play();
            Debug.Log("Hear the sound");
        }
        //s.source.clip = s.clip;
    }

    public void SwitchMusicState(bool isEnabled)
    {
        if(sources[0].clip != null)
        {
            if (isEnabled)
            {
                sources[0].Play();
            }
            else
            {
                sources[0].Pause();
            }
        }
    }

    public void SetMusicVolume(float f)
    {
        Debug.Log("Volume changed");
        sources[0].volume = f;
    }

    public void SetSoundVolume(float f)
    {
        sources[1].volume = f;
    }

    public float GetMusicVolume()
    {
        return sources[0].volume;
    }

    public float GetSoundVolume()
    {
        return sources[1].volume;
    }



}
