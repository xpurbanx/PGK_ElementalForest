﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TurnManager : MonoBehaviour
{
    public static TurnManager Instance;
    public GameManager gameManager;
    public UnitManager unitManager;
    public TurnType turnType { get; private set; }

    private float waitTime = 1f;
    public int turnCounter;
    public float blinkTimer = 0.2f;
    private IEnumerator turnChecking;
    private Coroutine currentTurnCheckRoutine;

    private IEnumerator enemyEnabling;
    private Coroutine currentEnemyEnablerRoutine;

    private IEnumerator buttonBlinking;
    private Coroutine blinkingButtonRoutine;

    private bool buttonBlink = false;
    private bool blinkState = false;

    public enum TurnType
    {
        Player, Enemies, EndTurn
    }

    private void Awake()
    {
        Instance = this;
        turnChecking = TurnCheckingRoutine();
        enemyEnabling = EnemyEnablingRoutine();
        buttonBlinking = ButtonBlinking();
    }

    public void NewTurnRecovery()
    {
        UIManager.Instance.AddBattleLog("TURN: " + turnCounter.ToString());
        Click.Instance.DeselectObject();
        gameManager.map.ClearHexSelections();
        
        foreach (Unit unit in unitManager.playerUnits)
        {
            unit.TurnStartRecovery();
        }
        foreach (Unit unit in unitManager.enemyUnits)
        {
            unit.TurnStartRecovery();
        }
        gameManager.map.UpdateAllHexStatus();
    }

    public bool IsPlyerTurnFinished()
    {
        foreach (Unit unit in unitManager.playerUnits)
        {
            unit.CheckEndTurn();
            if (unit.turnEnded == false)
            {
                //     Debug.Log("Player turn not finished!");
                return false;
            }

        }
        //Debug.Log("Player turn finished!");
        return true;
    }

    public bool IsEnemyTurnFinished()
    {
        foreach (Unit unit in unitManager.enemyUnits)
        {
            unit.CheckEndTurn();
            if (unit.turnEnded == false)
            {
                //  Debug.Log("Enemy turn not finished!");
                return false;
            }

        }
        // Debug.Log("Enemy turn finished!");
        return true;
    }

    public IEnumerator TurnCheckingRoutine()
    {
        while (gameManager.isGameActive)
        {
            yield return new WaitForSeconds(waitTime);
            if (turnType == TurnType.Player && IsPlyerTurnFinished())
            {
                buttonBlink = true;
                blinkingButtonRoutine = StartCoroutine(buttonBlinking);
                //UIManager.Instance.turnTypeText.text = "Turn: Enemy";
                //ColorBlock colorBlock = UIManager.Instance.endTurnButton.GetComponent<Button>().colors;
                //colorBlock.normalColor = new Color(0.8f,0.2f,0.2f);
                //colorBlock.highlightedColor = new Color(0.8f, 0.2f, 0.2f);
                //colorBlock.pressedColor = new Color(0.8f, 0.2f, 0.2f);
                //colorBlock.selectedColor = new Color(0.8f, 0.2f, 0.2f);
                //UIManager.Instance.endTurnButton.GetComponent<Button>().colors = colorBlock;
                //turnType = TurnType.Enemies;
                //StartEnemyEnabling();
            }
            else
            {
                buttonBlink = false;
                StopCoroutine(buttonBlinking);
            }
            // else continue;

            if (turnType == TurnType.Enemies && IsEnemyTurnFinished())
            {
                //UIManager.Instance.turnTypeText.text = "Turn: End";
                StopEnemyEnabling();
                turnType = TurnType.EndTurn;
                //gameManager.map.UpdateAllHexStatus();
                //yield return new WaitForSeconds(0.5f);
               
                //gameManager.map.UpdateAllHexStatus();
            }
            //else continue;

            if (turnType == TurnType.EndTurn)
            {
                //UIManager.Instance.turnTypeText.text = "Turn: Player";
                ++turnCounter;
                //  Debug.Log("Now is turn:" + turnCounter);
                UIManager.Instance.turnText.text = "Turn number: " + turnCounter;
                ColorBlock colorBlock = UIManager.Instance.endTurnButton.GetComponent<Button>().colors;
                colorBlock.normalColor = new Color(0.78f, 0.85f, 0.36f);
                colorBlock.highlightedColor = new Color(0.64f, 0.69f, 0.30f);
                colorBlock.pressedColor = new Color(0.41f, 0.44f, 0.19f);
                colorBlock.selectedColor = new Color(0.51f, 0.55f, 0.24f);
                UIManager.Instance.endTurnButton.GetComponent<Button>().colors = colorBlock;
                turnType = TurnType.Player;

                if (unitManager.mainTree)
                    NewTurnRecovery();
            }
            //Debug.Log("Turn Checked!");
        }

    }

    public IEnumerator EnemyEnablingRoutine()
    {
        while (turnType == TurnType.Enemies)
        {
            Goblin goblin;
            for (int i = 0; i < unitManager.enemyUnits.Count; i++)
            {
                goblin = unitManager.enemyUnits[i].GetComponent<Goblin>();
                goblin.isTakingTurn = true;
                goblin.canReachTarget = true;
                goblin.currentTurnTimer = 0f;
                //goblin.GetHexBelow();
                goblin.StartMakingDecisions();
                while (goblin.isTakingTurn)
                {
                    goblin.currentTurnTimer += 0.5f;
                    yield return new WaitForSeconds(0.5f);
                    if (goblin == null)
                        goblin.isTakingTurn = false;
                }

            }
            //foreach (Goblin unit in unitManager.enemyUnits)
            //{

            //}
            yield return new WaitForSeconds(0.1f);

        }

    }

    public IEnumerator ButtonBlinking()
    {
        ColorBlock colorBlock = UIManager.Instance.endTurnButton.GetComponent<Button>().colors;
        while (buttonBlink)
        {
            if (blinkState)
            {
                colorBlock.normalColor = new Color(0.8f, 1.0f, 0.8f);
                UIManager.Instance.endTurnButton.GetComponent<Button>().colors = colorBlock;
                UIManager.Instance.endTurnButton.GetComponent<Button>().transform.DOScale(1.3f, 0.5f);
                blinkState = false;
            }
            else
            {
                //ColorBlock colorBlock = UIManager.Instance.endTurnButton.GetComponent<Button>().colors;
                colorBlock.normalColor = new Color(0.78f, 0.85f, 0.36f);
                UIManager.Instance.endTurnButton.GetComponent<Button>().colors = colorBlock;
                UIManager.Instance.endTurnButton.GetComponent<Button>().transform.DOScale(1.0f, 0.5f);
                blinkState = true;
            }
            yield return new WaitForSeconds(blinkTimer);
        }

    }

    public void StartTurnChecking()
    {
        currentTurnCheckRoutine = StartCoroutine(turnChecking);
    }

    public void StartEnemyEnabling()
    {
        //StopEnemyEnabling();
        currentEnemyEnablerRoutine = StartCoroutine(enemyEnabling);
    }

    public void StopEnemyEnabling()
    {
        if (currentEnemyEnablerRoutine != null)
            StopCoroutine(currentEnemyEnablerRoutine);
    }

    public void EnemyTurnEnabler()
    {
        currentTurnCheckRoutine = StartCoroutine(turnChecking);
    }

    public void EndPlayerTurn()
    {
        foreach (Unit unit in unitManager.playerUnits)
        {
            unit.EndTurn();
        }

        ColorBlock colorBlock = UIManager.Instance.endTurnButton.GetComponent<Button>().colors;
        colorBlock.normalColor = new Color(0.8f, 0.2f, 0.2f);
        colorBlock.highlightedColor = new Color(0.8f, 0.2f, 0.2f);
        colorBlock.pressedColor = new Color(0.8f, 0.2f, 0.2f);
        colorBlock.selectedColor = new Color(0.8f, 0.2f, 0.2f);
        if (blinkingButtonRoutine != null)
            StopCoroutine(blinkingButtonRoutine);
        UIManager.Instance.endTurnButton.GetComponent<Button>().colors = colorBlock;
        UIManager.Instance.endTurnButton.GetComponent<Button>().transform.DOScale(1.0f, 0.5f);
        turnType = TurnType.Enemies;
        StartEnemyEnabling();
    }

    public void EndEnemiesTurn()
    {
        foreach (Unit unit in unitManager.enemyUnits)
        {
            unit.EndTurn();
        }
    }

    public bool DiceEncounter(OBJECT_TYPE objectType)
    {
        if (objectType != OBJECT_TYPE.OtherDeco) { return false; }
        //here will be randomized from 0 to SpawnProbability-1 but I have no idea so now
        if (new System.Random().Next(16) == 15) { return true; }
        else { return false; }
    }

}
