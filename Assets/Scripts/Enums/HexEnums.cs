﻿
public enum TERRAIN_TYPE
{
    GRASS, 
    BARREN, 
    WATER,
    EARTH

}
public enum FEATURE_TYPE 
{
    NONE,
    LIGHTFOREST,
    ROCK,
    FOREST

}

public enum EFFECT_TYPE 
{ 
    NONE,
    FIRE,
    COLD,
    CORRUPTION,
    SMOKE

}


