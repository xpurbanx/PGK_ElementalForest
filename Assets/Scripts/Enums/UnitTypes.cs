﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OBJECT_TYPE
{
    None,
    MainTree,
    Treant,
    Seed,
    AirElemental,
    EarthElemental,
    FireElemental,
    StormElemental,
    WaterElemental,
    GoblinWarrior,
    GoblinSoldier,
    GoblinRider,
    Rock,
    LightForest,
    DenseForest,
    OtherDeco,
    GoblinAxe,
}

public enum ELEMENTAL_TYPE
{
    Air,
    Earth,
    Water,
    Fire,
    Storm,
    Treant
}
