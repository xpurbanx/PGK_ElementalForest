﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// Handles mouse clicks
public class Click : MonoBehaviour
{
    public static Click Instance;
    public GameManager gameManager;
    // Makes only the player's units selectable
    [SerializeField]
    private LayerMask PlayerUnit;

    // Makes the map tiles clickable
    [SerializeField]
    private LayerMask hexTile;
    [SerializeField]
    private LayerMask UIMask;

    private MapObject selectedObject;
    public MapObject targetedObject;
    private UnitController selectedController;

    public List<HexComponent> allowedHexes;
    private HexComponent selectedHex;
    public HexComponent targetedHex;
    TooltipTrigger tooltipTrigger = null;
    public enum CLICK_MODE { All, TargetHex, SelectHex, TargetObject, SelectObject, MoveCommand }
    CLICK_MODE clickMode { get; set; }
    public event EventHandler UnitMovementFinished;

    private void Start()
    {
        gameManager = UIManager.Instance.gameManager;
        //clickMode = CLICK_MODE.All;
        Instance = this;
    }
    // Update is called once per frame
    // Selects an object with a LMB click and deselects any previous selected object
    // Deselects all objects with a clicking at nothing or hex
    // If a unit is selected, clicking RMB on a hex sets it as destination
    void Update()
    {

        SelectHex();
        MouseOverObject();
        //select with LBM
        if (Input.GetMouseButtonDown(0))
        {
            //blocks clicking through UI
            if (!EventSystem.current.IsPointerOverGameObject())
            {

                // DeselectHexClick();
                //  DeselectObject();

                if (clickMode == CLICK_MODE.All || clickMode == CLICK_MODE.MoveCommand || clickMode == CLICK_MODE.SelectObject)
                {
                    //DeselectHexClick();
                   // DeselectObject();
                    SelectObject();
                    RemoveTarget();
                }
                else if (clickMode == CLICK_MODE.All || clickMode == CLICK_MODE.SelectHex)
                {
                    //DeselectObject();
                    SelectHex();
                    RemoveTarget();
                }
                else if (clickMode == CLICK_MODE.TargetObject)
                {
                    TargetObject();
                }
                else if (clickMode == CLICK_MODE.TargetHex)
                {
                    TargetHex();
                }

                if (clickMode == CLICK_MODE.MoveCommand)
                {
                    MoveCommand();
                }
            }

        }
        //order to move with RMB
        if (Input.GetMouseButtonDown(1))
        {
            DeselectHexClick();
            DeselectObject();
            clickMode = CLICK_MODE.All;
            // MoveCommand();
        }
    }
    public void ChangeClickMode(CLICK_MODE mode)
    {
        clickMode = mode;
    }

    public MapObject GetTargetObject()
    {
        return targetedObject;
    }
    /// <summary>
    /// Targets MapObjects by clicking on them
    /// </summary>
    /// <returns>Return true if new target was selected succesfuly</returns>
    bool TargetObject()
    {
        ClearHighlightAvailableHexes();
        // if (physicsRaycaster.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("MapObject")))
        RaycastHit rayHit;
        //if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("MapObject")))
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, hexTile))
        {
            //targetedObject.GetHexBelow();
            targetedHex = rayHit.collider.gameObject.GetComponent<HexComponent>();
            if (targetedHex.mapObject == null)
            {
                return false;
            }

            targetedObject = targetedHex.mapObject;

            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Targets MapObjects by clicking on them
    /// </summary>
    /// <returns>Return true if new target was selected succesfuly</returns>
    bool TargetHex()
    {
        ClearHighlightAvailableHexes();
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, hexTile))
        {
            targetedHex = rayHit.collider.gameObject.GetComponent<HexComponent>();
            return true;
        }
        else
            return false;
    }

    public void RemoveTarget()
    {
        targetedObject = null;
        targetedHex = null;
    }

    bool MouseOverObject()
    {
        MapObject mouseOverObject;
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("MapObject")))
        {
            mouseOverObject = rayHit.collider.gameObject.GetComponent<MapObject>();
            UIManager.Instance.UpdateUnitMouseOver(mouseOverObject);

            //   if (mouseOverObject.GetComponent<TooltipTrigger>() != null)
            //  {
            if (mouseOverObject != null)
                mouseOverObject.TryGetComponent(out tooltipTrigger);
            // tooltipTrigger = mouseOverObject.TryGetComponent<TooltipTrigger>();

            //    }

            if (tooltipTrigger != null)
            {
                transform.DOShakeScale(0.01f, 0).SetDelay(1).OnComplete(() =>
                {
                    TooltipSystem.Show(tooltipTrigger.content, tooltipTrigger.header);

                });
            }
            return true;
        }
        else
        {
            if (tooltipTrigger != null)
            {
                TooltipSystem.Hide();
                transform.DOKill();
                tooltipTrigger = null;
            }

            mouseOverObject = null;
            UIManager.Instance.UpdateUnitMouseOver(mouseOverObject);
            return false;
        }
    }
    /// <summary>
    /// Selects MapObjects by clicking on them
    /// </summary>
    /// <returns>Return true if new object was selected succesfuly</returns>
    bool SelectObject()
    {
        UIManager.Instance.ClearPrompt();

        RaycastHit rayHit;
        //if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, LayerMask.GetMask("MapObject")))
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, hexTile))
        {
            //makes sure that last selected object is not clicked, might need to change this

            HexComponent tmpHex = rayHit.collider.gameObject.GetComponent<HexComponent>();
            if (tmpHex.mapObject == null)
            {
                return false;
            }
            if (selectedObject != null && tmpHex.mapObject != null)
            {
                DeselectObject();
            }
            selectedObject = tmpHex.mapObject;
            selectedObject.GetHexBelow();
            selectedObject.isClicked = true;
            UIManager.Instance.UpdateUnitMouseOver(selectedObject);
            if (selectedObject is Unit)
            {
                selectedController = selectedObject.GetComponent<UnitController>();
                selectedController.unit.StatusChanged += OnUnitStatusChanged;
                UIManager.Instance.UpdateUnitUIInfo(selectedController.unit);
                CheckSelectedUnitMovement();

                //selectedObject.GetComponent<UnitController>().Select();
                selectedController.Select();
                clickMode = CLICK_MODE.MoveCommand;
            }
            return true;
        }
        else
            return false;
    }
    /// <summary>
    /// Selects hex tile
    /// </summary>
    /// <returns>Returns true if new object was selected succesfuly</returns>
    bool SelectHex()
    {
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, hexTile))
        {
            DeselectHex();

            selectedHex = rayHit.collider.gameObject.GetComponent<HexComponent>();
            selectedHex.OutlineSelected();
            // if (selectedHex.mapObject != null)
            // {
            // UIManager.Instance.UpdateUnitMouseOver(selectedHex.mapObject);
            //  }

            //displays distance to other hexes
            //foreach (HexComponent hex in gameManager.map.hexes)
            //{
            //    if (selectedObject is Unit)
            //    {

            //    }
            //    hex.DistanceToSelected(selectedHex);
            //}

            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Deselects current hex tile by not clicking tile
    /// </summary>
    /// <returns>Returns true if new object was selected succesfuly</returns>
    void DeselectHexClick()
    {
        LayerMask layerMask = ~(1 << hexTile & 1 << UIMask);
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, ~hexTile))
        {
            if (selectedHex != null)
            {
                selectedHex.OutlineDefault();
                selectedHex = null;
            }
        }
    }

    /// <summary>
    /// Deselects current hex tile
    /// </summary>
    /// <returns>Returns true if new object was selected succesfuly</returns>
    void DeselectHex()
    {

        if (selectedHex != null)
        {
            selectedHex.OutlinePrevious();
            selectedHex = null;
            //clickMode = CLICK_MODE.All;
        }

    }

    /// <summary>
    /// Deselects current object if we click anything else
    /// </summary>
    public void DeselectObject()
    {
        LayerMask layerMask = ~(1 << LayerMask.GetMask("MapObject") & 1 << UIMask);
        RaycastHit rayHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, layerMask))
        {
            if (rayHit.transform.gameObject.layer == LayerMask.GetMask("UI"))
            {
                Debug.Log("UI Layer hit!");
                return;

            }
            if (selectedObject != null)
            {
                if (selectedController != null)
                {
                    selectedController.unit.StatusChanged -= OnUnitStatusChanged;
                    UIManager.Instance.UnitUIOff();
                    ClearHighlightAvailableHexes();
                    gameManager.map.ClearHexSelections();
                    selectedController.Deselect();
                    selectedController.currentlyIsSelected = false;
                    foreach (Ability ability in selectedController.unit.abilityList.abilities)
                    {
                        //ability.targeting.HideRange();
                    }
                    selectedController = null;

                }
                //selectedObject.isClicked = false;
                selectedObject = null;
                clickMode = CLICK_MODE.All;
            }
        }
    }

    /// <summary>
    /// If unit is selected, move this unit by right clicking on tile
    /// </summary>
    void MoveCommand()
    {
        //selects tile to move to, ignore current tile unit is standing on
        // temporaily disabled checking if tile has another object
        if (selectedController != null && SelectHex() == true && selectedHex != selectedObject.hexComponent && selectedController.unit.isMovementFinished)// && selectedHex.hex.hasObject == false)
        {


            if (selectedController.unit.factionID == MapObject.FACTION_ID.PLAYER)
            {
                int distance = (int)HexAccessor.Distance(selectedController.unit.hex, selectedHex.hex);
                if (!allowedHexes.Contains(selectedHex))
                {
                    UIManager.Instance.promptText.text = "Hex too far, cant reach this turn!";
                    DeselectObject();
                    clickMode = CLICK_MODE.All;
                    return;
                }
                else
                {
                    ClearHighlightAvailableHexes();
                    selectedController.unit.Move(selectedHex);
                    CheckSelectedUnitMovement();

                }

            }
        }


    }

    IEnumerator UnitMoving(List<HexComponent> hexPath)
    {
        float waitTime = 0.2f;
        foreach (HexComponent hexComp in hexPath)
        {
            selectedController.unit.Move(hexComp);
            selectedController.unit.isMoving = true;
            yield return new WaitForSeconds(waitTime);
        }
        yield return null;
    }

    void OnUnitStatusChanged(object sender, EventArgs e)
    {
        ClearHighlightAvailableHexes();
        CheckSelectedUnitMovement();
        UIManager.Instance.UpdateUnitUIInfo(selectedController.unit);
    }

    void CheckSelectedUnitMovement()
    {
        if (selectedController.unit.isMovementFinished)
        {
            allowedHexes.Clear();
            allowedHexes = HexAccessor.GetMovementHexes(selectedController.unit);
            allowedHexes = HexAccessor.CheckForObjects(allowedHexes);
            HighlightAvailableHexes();
        }

    }


    void HighlightAvailableHexes()
    {
        foreach (HexComponent hex in allowedHexes)
        {
            hex.OutlineHighlighted();
        }
    }

    void ClearHighlightAvailableHexes()
    {
        foreach (HexComponent hex in allowedHexes)
        {
            hex.OutlineDefault();
        }
    }

    public void OnUnitMovementFinished()
    {
        EventHandler handler = UnitMovementFinished;
        handler?.Invoke(this, EventArgs.Empty);
    }


}
