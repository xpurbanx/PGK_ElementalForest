﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using IngameDebugConsole;

public class DebugUI : MonoBehaviour
{
    public static DebugUI Instance;


    public GameObject debugMouseover;
    public TMP_Text mapObjectIDText;

    private void Awake()
    {
        if (DebugLogManager.Instance != null)
            Instance = this;
    }



}
