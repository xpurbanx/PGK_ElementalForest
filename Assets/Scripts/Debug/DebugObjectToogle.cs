﻿using IngameDebugConsole;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugObjectToogle : MonoBehaviour
{
    
    void Start()
    {
        if(DebugLogManager.Instance.enabled == false)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }


}
