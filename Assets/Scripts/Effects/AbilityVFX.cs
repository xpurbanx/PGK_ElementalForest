﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AbilityVFX : MonoBehaviour
{
    public GameObject target;
    public GameObject caster;

    public ParticleSystem particles;
    ParticleSystem effect;
    public float doTweenDuration = 1.0f;

    public void Initialize(GameObject _target, GameObject _caster)
    {
        target = _target;
        caster = _caster;
    }
    public void PlayOnTarget()
    {
        effect = Instantiate(particles, target.transform);
        effect.transform.position = target.transform.position;
        effect.Play();

        Destroy(effect.gameObject, effect.main.duration);
    }

    public void PlayOnSelf()
    {
        effect = Instantiate(particles, target.transform);
        effect.transform.position = caster.transform.position;
        effect.Play();

        Destroy(effect.gameObject, effect.main.duration);
    }

    public void PlayTweenTarget()
    {
        effect = Instantiate(particles, caster.transform.position,Quaternion.identity, caster.transform);
        //effect.transform.position = caster.transform.position;
        effect.transform.DOMove(target.transform.position, doTweenDuration);
        effect.Play();

        Destroy(effect.gameObject, effect.main.duration);
    }

    public void PlayJumpTarget()
    {
        effect = Instantiate(particles, caster.transform.position, Quaternion.identity, caster.transform);
        //effect.transform.position = caster.transform.position;
        effect.transform.DOMove(target.transform.position, doTweenDuration);
        effect.Play();

        Destroy(effect.gameObject, effect.main.duration);
    }

    public static void ThrowObject(GameObject throwable, Vector3 endPos)
    {
        throwable.transform.DOJump(endPos, 1f, 1, 1f);
    }
}
