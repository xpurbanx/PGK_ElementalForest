﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;

public class Map : MonoBehaviour
{
    public GameObject hexTilePrefab;
    public GameObject hexHolder;
    public int columns = 20;
    public int rows = 20;
    #region materials
    public Material MatWater;
    public Material MatGrass;
    public Material MatEarth;
    #endregion
    #region feature objects
    public GameObject rock;
    #endregion

    public MapData mapData;
    public List<HexComponent> hexes;
    public Dictionary<Hex, GameObject> hexToGameObjectMap;
    public Dictionary<Hex, HexComponent> hexToHexComponentMap;
    public Dictionary<GameObject, Hex> gameObjectToHexMap;

    public string mapName;
    string savePath;
    public GameObject playerUnitsHolder;
    public GameObject enemyUnitsHolder;
    public GameObject mapFeaturesHolder;
    public MapObjectPrefabs unitPrefabs;
    //creates new hex information
    private void Awake()
    {
        DestroyMap();
        BuildMap();
    }
    private void Start()
    {
        ClearHexSelections();
    }

    public void ClearHexSelections()
    {
        foreach (HexComponent hex in hexes)
        {
            hex.OutlineDefault();
        }
    }

    /// <summary>
    /// Returns hex tile at given coordinates.
    /// </summary>
    /// <param name="x">Column number of hex</param>
    /// <param name="y">Row number of hex</param>
    /// <returns></returns>
    public Hex GetHexAt(int x, int y)
    {
        if (mapData.hexes == null)
        {
            Debug.LogError("Hexes array not yet instantiated.");
            return null;
        }
        //need to do it in try catch, becouse some hexes may not have all neighours (map edges)
        try
        {
            return mapData.hexes[y + (x * mapData.rowsNumber)];
        }
        catch
        {
            //Debug.LogError("GetHexAt: " + x + "," + y);
            return null;
        }
    }

    public void BuildMap()
    {
        //hexes.Clear();
        hexes = new List<HexComponent>();
        DestroyMap();
        if (mapData == null)
        {
            mapData = new MapData();
            mapData.hexes = new Hex[columns * rows];
            mapData.columnsNumber = columns;
            mapData.rowsNumber = rows;
            mapData.mapName = mapName;
            //create needed hexes
            for (int column = 0; column < columns; column++)
            {
                for (int row = 0; row < rows; row++)
                {
                    Hex hex = new Hex(column, row);
                    mapData.hexes[row + (column * rows)] = hex;
                }
            }
        }
        else
        {
            //map was loaded with existing mapData and hexes
            rows = mapData.rowsNumber;
            columns = mapData.columnsNumber;
            mapName = mapData.mapName;
        }

        hexToGameObjectMap = new Dictionary<Hex, GameObject>();
        gameObjectToHexMap = new Dictionary<GameObject, Hex>();
        hexToHexComponentMap = new Dictionary<Hex, HexComponent>();
        for (int column = 0; column < columns; column++)
        {
            for (int row = 0; row < rows; row++)
            {
                //Debug.Log("Column: " + column + "  Row: " + row);
                Hex hex = mapData.hexes[row + (column * rows)];
                //Vector3 pos = hex.Position();
                Vector3 pos = new Vector3(
                    Hex.HexHorizontalSpacing() * (hex.Q + hex.R / 2f),
                    0,
                    Hex.HexVerticalSpacing() * hex.R
                );

                GameObject hexGameObject = (GameObject)Instantiate(
                    hexTilePrefab,
                    pos,
                    Quaternion.identity,
                    hexHolder.transform
                );
                hexGameObject.layer = LayerMask.NameToLayer("HexTile");
                hexToGameObjectMap[hex] = hexGameObject;
                gameObjectToHexMap[hexGameObject] = hex;

                hexGameObject.name = string.Format("HEX: {0},{1}", column, row);
                HexComponent hexComponent = hexGameObject.GetComponent<HexComponent>();
                hexComponent.hex = hex;
                hexComponent.map = this;
                hexComponent.UpdateTerrain();
                hexToHexComponentMap.Add(hex, hexComponent);
                hexes.Add(hexComponent);

                if (hex.objectOnHex != OBJECT_TYPE.None)
                {
                    foreach (var prefabObject in unitPrefabs.objects)
                    {
                        MapObject mapObject = prefabObject.GetComponent<MapObject>();
                        if (mapObject.objectType == hex.objectOnHex)
                        {
                            if (mapObject.factionID == MapObject.FACTION_ID.PLAYER)
                                Instantiate(prefabObject, hexComponent.transform.position, hexComponent.transform.rotation, playerUnitsHolder.transform);
                            else if (mapObject.factionID == MapObject.FACTION_ID.ENEMY)
                            {
                                Instantiate(prefabObject, hexComponent.transform.position, hexComponent.transform.rotation, enemyUnitsHolder.transform);
                            }
                            else
                            {
                                Instantiate(prefabObject, hexComponent.transform.position, hexComponent.transform.rotation, mapFeaturesHolder.transform);
                            }

                            hexComponent.mapObject = mapObject;
                            mapObject.hexComponent = hexComponent;
                            mapObject.hex = hex;
                            mapObject.hex.objectOnHex = mapObject.objectType;
                            //need to make it separate later
                            if (mapObject is MapFeature)
                            {
                                hexComponent.hex.FeatureType = mapObject.GetComponent<MapFeature>().featureType;
                            }
                        }
                    }
                }

                //for walkable map features
                if (hex.objectOnHex == OBJECT_TYPE.None && hex.FeatureType != FEATURE_TYPE.NONE)
                {
                    foreach (var prefabObject in unitPrefabs.objects)
                    {
                        MapObject mapObject = prefabObject.GetComponent<MapObject>();
                        if (mapObject is MapFeature)
                        {

                            MapFeature mapFeature = prefabObject.GetComponent<MapFeature>();
                            if (mapFeature.featureType == hex.FeatureType)
                            {
                                Instantiate(prefabObject, hexComponent.transform.position, hexComponent.transform.rotation, mapFeaturesHolder.transform);

                                mapFeature.hexComponent = hexComponent;
                                mapFeature.hex = hex;
                                mapFeature.hex.FeatureType = mapFeature.featureType;
                            }
                        }
                    }
                }
            }
        }
        UpdateAllHexVisuals();
        UnitManager.Instance.mainTree = true; // Is set to false when map is destroyed
    }
    public void DestroyMap()
    {
        for (int i = hexHolder.transform.childCount; i > 0; --i)
            DestroyImmediate(hexHolder.transform.GetChild(0).gameObject);
        for (int i = playerUnitsHolder.transform.childCount; i > 0; --i)
            DestroyImmediate(playerUnitsHolder.transform.GetChild(0).gameObject);
        for (int i = enemyUnitsHolder.transform.childCount; i > 0; --i)
            DestroyImmediate(enemyUnitsHolder.transform.GetChild(0).gameObject);
        for (int i = mapFeaturesHolder.transform.childCount; i > 0; --i)
            DestroyImmediate(mapFeaturesHolder.transform.GetChild(0).gameObject);
    }

    //mapData should contain hex information already
    public void LoadMap(string name)
    {
        mapData = SaveSystem.LoadMap(name);
        if (mapData == null)
        {
            Debug.LogWarning("Map not loaded!");
            return;
        }
        BuildMap();
    }

    //mapData should contain hex information already
    public void CreateNewMap()
    {
        mapData = null;
        BuildMap();
    }

    public void UpdateAllHexStatus()
    {
        foreach (HexComponent hex in hexes)
        {
            hex.UpdateHexStatus();
        }
    }

    public void UpdateSingleHexVisual(HexComponent hex)
    {
        MeshRenderer mr = hex.GetComponentInChildren<MeshRenderer>();
        MeshFilter mf = hex.GetComponentInChildren<MeshFilter>();

        switch (hex.hex.TerrainType)
        {
            case TERRAIN_TYPE.WATER:
                mr.material = MatWater;
                break;
            case TERRAIN_TYPE.GRASS:
                mr.material = MatGrass;
                break;
            case TERRAIN_TYPE.EARTH:
                mr.material = MatEarth;
                break;
        }
    }

    public void UpdateAllHexVisuals()
    {
        for (int column = 0; column < columns; column++)
        {
            for (int row = 0; row < rows; row++)
            {
                Hex hex = mapData.hexes[row + (column * rows)];
                GameObject hexGameObject = hexToGameObjectMap[hex];

                HexComponent hexComp = hexGameObject.GetComponentInChildren<HexComponent>();
                MeshRenderer mr = hexGameObject.GetComponentInChildren<MeshRenderer>();
                MeshFilter mf = hexGameObject.GetComponentInChildren<MeshFilter>();

                switch (hex.TerrainType)
                {
                    case TERRAIN_TYPE.WATER:
                        mr.material = MatWater;
                        break;
                    case TERRAIN_TYPE.GRASS:
                        mr.material = MatGrass;
                        break;
                    case TERRAIN_TYPE.EARTH:
                        mr.material = MatEarth;
                        break;
                }
                //hexComp.UpdateTerrain();


            }
        }
    }
}
