﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapData 
{
    public string mapName;

    public Hex[] hexes;
    //public Hex[,] hexes;
    public int columnsNumber;
    public int rowsNumber;
}
