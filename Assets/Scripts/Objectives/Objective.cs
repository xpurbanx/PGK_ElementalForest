﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Objective : MonoBehaviour
{
    public string title;
    public string description;
    public int order;

    public Objective() { }

    public abstract bool SuccessConditions();

    public abstract bool FailConditions();

    public abstract void Reward();

    public abstract void UpdateDescription();
}
