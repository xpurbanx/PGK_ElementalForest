﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OClear : Objective
{
    public void Start()
    {
        name = "Kill all goblins";
    }
    
    public override bool FailConditions()
    {
        // None
        return false; 
    }

    public override void Reward()
    {
        // None
    }

    public override bool SuccessConditions()
    {
        if (UnitManager.Instance.enemyUnits.Count == 0)
        {
            Debug.Log("Enemies killed!");
            return true;
        }
        return false;
    }

    public override void UpdateDescription()
    {
        name = "Kill all goblins";
        description = string.Concat("Kill the remaining ", UnitManager.Instance.enemyUnits.Count.ToString(), " goblins to win");
    }
}
