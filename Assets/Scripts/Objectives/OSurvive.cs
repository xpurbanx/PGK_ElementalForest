﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSurvive : Objective
{
    public int turnLimiter;

    public override bool FailConditions()
    {
        // None
        return false;
    }

    public override void Reward()
    {
        // None
    }

    public override bool SuccessConditions()
    {
        if (TurnManager.Instance.turnCounter >= turnLimiter)
            return true;
        return false;
    }

    public override void UpdateDescription()
    {
        description = string.Concat("Survive for ", (turnLimiter - TurnManager.Instance.turnCounter).ToString(), " turns");
    }

    // Start is called before the first frame update
    void Start()
    {
        name = "Survive long enough";
    }

}
