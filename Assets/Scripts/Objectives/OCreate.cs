﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OCreate : Objective
{
    public OBJECT_TYPE requiredObject;

    // Start is called before the first frame update
    void Start()
    {
        name = "Create a " + requiredObject.ToString();
    }

    public override bool FailConditions()
    {
        // None
        return false;
    }

    public override void Reward()
    {
        // None
    }

    public override bool SuccessConditions()
    {
        foreach (var unit in UnitManager.Instance.playerUnits)
        {
            if (unit.objectType == requiredObject)
            {
                return true;
            }
        }
        return false;
    }

    public override void UpdateDescription()
    {
        name = "Create a " + requiredObject.ToString();
        description = string.Concat("You need to create a ", requiredObject.ToString(), " to progress");
    }
}
