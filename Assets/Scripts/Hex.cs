﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Hex 
{
    public Hex(int q, int r)
    {
        this.Q = q;
        this.R = r;
        this.S = -(q + r);
        TerrainType = TERRAIN_TYPE.WATER;
        FeatureType = FEATURE_TYPE.NONE;
        objectOnHex = OBJECT_TYPE.None;
        effects = new List<EFFECT_TYPE>();
    }
    // Q + R + S = 0
    // S = -(Q + R)
    public int Q; // Column
    public int R;   // Row
    public int S;

    public TERRAIN_TYPE TerrainType;
    public FEATURE_TYPE FeatureType;
    public List<EFFECT_TYPE> effects;
    public OBJECT_TYPE objectOnHex;
    public bool hasObject;
    public bool isWalkable;
    public bool isFlyable;
    public bool isSwimmable;

    public static float WIDTH_MULTIPLIER = Mathf.Sqrt(3) / 2;
    public static float radius = 0.5f;
    public static float HexHeight()
    {
        return radius * 2;
    }

    public static float HexWidth()
    {
        return WIDTH_MULTIPLIER * HexHeight();
    }

    public static float HexVerticalSpacing()
    {
        return HexHeight() * 0.75f;
    }

    public static float HexHorizontalSpacing()
    {
        return HexWidth();
    }

    //public static float Distance(Hex a, Hex b)
    //{
    //    int dQ = Mathf.Abs(a.Q - b.Q);
    //    int dR = Mathf.Abs(a.R - b.R);
    //    return
    //        Mathf.Max(
    //            dQ,
    //            dR,
    //            Mathf.Abs(a.S - b.S)
    //        );
    //}

    /// <summary>
    ///  Returns array of neighbouring hexes
    /// </summary>
    /// <param name="map">Map component that contains MapData</param>
    /// <returns></returns>
 

}
