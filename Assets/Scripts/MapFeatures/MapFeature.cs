﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFeature : MapObject
{
    public FEATURE_TYPE featureType;

    public override void Awake()
    {
        base.Awake();

    }

    protected override void Start()
    {
        base.Start();
        if (featureType == FEATURE_TYPE.FOREST || featureType == FEATURE_TYPE.LIGHTFOREST)
        {
            UnitManager.Instance.AddTree(this);
        }
        else
        {
            UnitManager.Instance.AddMapFeature(this);
        }
        StartCoroutine(LateStart(0.7f));
    }
    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GetHexBelow();
        hexComponent.mapFeature = this;
        if (AllowedFeaturesToStand(featureType))
        {
            hexComponent.ClearObjectInfo();
        }
    }
    public override void Die()
    {
        base.Die();
        if (featureType == FEATURE_TYPE.FOREST)
        {
            hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.LightForest));
            UnitManager.Instance.RemoveTree(this);
            hexComponent.hex.FeatureType = FEATURE_TYPE.LIGHTFOREST;
            hexComponent.hex.objectOnHex = OBJECT_TYPE.None;
        }
        else
        {
            UnitManager.Instance.RemoveTree(this);
            hexComponent.hex.FeatureType = FEATURE_TYPE.NONE;
            hexComponent.mapFeature = null;
        }

    }


    public static bool AllowedFeaturesToStand(FEATURE_TYPE feature)
    {
        if (feature == FEATURE_TYPE.NONE)
            return true;

        if (feature == FEATURE_TYPE.LIGHTFOREST)
            return true;

        return false;
    }

}
