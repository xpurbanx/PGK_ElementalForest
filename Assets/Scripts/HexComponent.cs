﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HexComponent : MonoBehaviour
{
    public Hex hex;
    public Map map;
    //this contains reference to unit standing on hex 
    public MapObject mapObject;

    public Material matOutlineDefault;
    public Material matOutlineSelected;
    public Material matOutlineHighlighted;
    public Material matOutlineRange;



    Material lastMaterial;

    public int distanceFromSelected;
    public HexText hexText;

    public bool isBurning { get; set; }
    public GameObject burningEffect;

    public List<MapObject> mapObjects = new List<MapObject>();

    public GameObject stormElemental;
    public bool isHighlighted;
    public bool isInRange;

    public MapFeature mapFeature;

    private MeshRenderer mr;
    [SerializeField]
    private SpriteRenderer sr;
    private MeshFilter mf;

    private void OnEnable()
    {
        hexText = GetComponentInChildren<HexText>();
        //sr = this.GetComponentInChildren<SpriteRenderer>();
        sr.enabled = true;
    }

    private void Awake()
    {
        //UpdateTerrain();
    }
    public void AddObjectInfo(MapObject target)
    {
        if (mapObject != null && mapObject.ID != 0)
        {
            Debug.Log(target.name + "was trying to add itself, but there is object: " + mapObject.name + "\n"
                + " targetID = " + target.ID + "    current object ID = " + mapObject.ID);
            return;
        }
          //  return;
        mapObject = target;
        hex.hasObject = true;
        hex.objectOnHex = target.objectType;
        target.hex = hex;
        target.hexComponent = this;

        if (mapObject is MapFeature)
        {
            MapFeature feature = (MapFeature) target;
            hex.FeatureType = feature.featureType;
        }
    }
    public void ClearObjectInfo()
    {
        mapObject = null;
        hex.hasObject = false;
        hex.objectOnHex = OBJECT_TYPE.None;
    }

    public void ClearObjectInfo(MapObject _mapObject)
    {
         if (mapObject == null)
            return;
        if (_mapObject.ID == mapObject.ID)
        {
            mapObject = null;
            hex.hasObject = false;
            hex.objectOnHex = OBJECT_TYPE.None;
            if (_mapObject is MapFeature)
            {
                MapFeature feature = (MapFeature) _mapObject;
                if (mapFeature != null && mapFeature.ID == feature.ID)
                {
                    mapFeature = null;
                }
                hex.FeatureType = FEATURE_TYPE.NONE;
            }
           // _mapObject.hex = null;
            //_mapObject.hexComponent = null;
        }
    }

    public void OutlineSelected()
    {
        lastMaterial = sr.material;
        sr.material = matOutlineSelected;
    }

    public void OutlineDefault()
    {
        isHighlighted = false;
        isInRange = false;
        if (sr == null)
        {
            //no idea why it is sometimes null
            return;
        }
        lastMaterial = sr.material;
        sr.material = matOutlineDefault;
    }

    public void OutlineHighlighted()
    {
        isHighlighted = true;
        sr.material = matOutlineHighlighted;

    }

    public void OutlineRange()
    {
        isInRange = true;
        sr.material = matOutlineRange;

    }

    /// <summary>
    /// Changes hex parameters depending on active effects
    /// </summary>
    public void UpdateHexStatus()
    {

        //damage goblins on fire hex
        if (isBurning && mapObject!= null && mapObject.factionID == MapObject.FACTION_ID.ENEMY)
        {
            mapObject.Damage(10);
        }

        //fire on grass will change it to earth
        if (isBurning && hex.FeatureType == FEATURE_TYPE.FOREST)
        {
            mapFeature.Damage(20);
            SpreadFire();
        }

        else if (isBurning && hex.FeatureType == FEATURE_TYPE.LIGHTFOREST)
        {
            mapFeature.Damage(15);
            SpreadFire();
        }

        else if (isBurning && hex.TerrainType == TERRAIN_TYPE.GRASS)
        {
            hex.TerrainType = TERRAIN_TYPE.EARTH;
            map.UpdateSingleHexVisual(this);
            StopFire();
        }

        //clears bugged hexes
        if(mapObject == null && hex.hasObject == true)
        {
            ClearObjectInfo();
        }


        

    }

    public void SpreadFire()
    {
        float chanceToSpread = Random.value;
        int spreadAmounts = 0;
        if (chanceToSpread < .55f) // 55% of the time
        {
            //no spread
            return;
        }
        else if (chanceToSpread < .9f) // 35% of the time
        {
            spreadAmounts = 1;
        }
        else // 10% of the time
        {
            spreadAmounts = 2;
        }
        List<HexComponent> neighbours = new List<HexComponent>();
        neighbours = HexAccessor.GetNeighbourHexComponents(this);
        List<HexComponent> hexesWithTrees = new List<HexComponent>();
        foreach (HexComponent hex in neighbours)
        {
            if (hex.mapFeature != null)
            {
                if (hex.mapFeature.featureType == FEATURE_TYPE.FOREST || hex.mapFeature.featureType == FEATURE_TYPE.LIGHTFOREST)
                    hexesWithTrees.Add(hex);
            }
        }

        for(int i = 0; i < spreadAmounts; i++)
        {
            if (hexesWithTrees.Count > 0)
            {
                hexesWithTrees[Random.Range(0, hexesWithTrees.Count)].StartFire();

            }
        }


    }

    public void StartFire()
    {
        if ((hex.FeatureType == FEATURE_TYPE.NONE || hex.FeatureType == FEATURE_TYPE.ROCK) && hex.TerrainType == TERRAIN_TYPE.EARTH)
            return;
        isBurning = true;
        burningEffect.gameObject.SetActive(true);
        if (!hex.effects.Contains(EFFECT_TYPE.FIRE))
            hex.effects.Add(EFFECT_TYPE.FIRE);

    }

    public void StopFire()
    {
        isBurning = false;
        burningEffect.gameObject.SetActive(false);
        if (hex.effects.Contains(EFFECT_TYPE.FIRE))
            hex.effects.Remove(EFFECT_TYPE.FIRE);

    }

    public void OutlinePrevious()
    {
        if (isInRange == true)
        {
            sr.material = matOutlineRange;

        }
        else if (isHighlighted == true)
        {
            sr.material = matOutlineHighlighted;

        }
        else
        {
            sr.material = matOutlineDefault;
        }

    }

    public void EnableHighlight()
    {
        sr.enabled = true;
    }

    public void DisableHighlight()
    {
        sr.enabled = false;
    }


    public int DistanceToSelected(HexComponent selectedHex)
    {
        distanceFromSelected = (int)HexAccessor.Distance(hex, selectedHex.hex);
        //hexText.SetDistanceText(distanceFromSelected);
        return distanceFromSelected;
    }

    public void UpdateTerrain()
    {
        switch (hex.TerrainType)
        {
            case TERRAIN_TYPE.WATER:
                //mr.material = map.MatWater;
                hex.isFlyable = true;
                hex.isSwimmable = true;
                hex.isWalkable = false;
                break;
            case TERRAIN_TYPE.GRASS:
                //mr.material = map.MatGrass;
                hex.isFlyable = true;
                hex.isSwimmable = false;
                hex.isWalkable = true;
                break;
            case TERRAIN_TYPE.EARTH:
                //mr.material = map.MatEarth;
                hex.isFlyable = true;
                hex.isSwimmable = false;
                hex.isWalkable = true;
                break;

        }
    }

    /// <summary>
    /// Instantiates given prefab on hex position
    /// </summary>
    /// <param name="gameObj">Prefab to spawn</param>
    public void SpawnObject(GameObject gameObj)
    {
        GameObject newObj = Instantiate(gameObj);
        newObj.transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);

        MapObject _mapObject = newObj.GetComponent<MapObject>();
        if (_mapObject != null)
        {
            ClearObjectInfo(_mapObject);
            AddObjectInfo(_mapObject);
        }
    }

    /// <summary>
    /// Instantiates given prefab on hex position
    /// </summary>
    /// <param name="gameObj">Prefab to spawn</param>
    public void SpawnCrystal(GameObject crystal, int manaLevel)
    {
        GameObject newObj = Instantiate(crystal);
        newObj.transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);

        MapObject _mapObject = newObj.GetComponent<MapObject>();
        if (_mapObject != null)
        {
            ClearObjectInfo(_mapObject);
            AddObjectInfo(_mapObject);
        }
        Seed cryst = newObj.GetComponent<Seed>();
        cryst.ChangeMana(manaLevel);
        if(cryst.manaCurrent == cryst.manaMax)
        {
            cryst.chargedParticles.SetActive(true);
        }

    }

    //gets the two map objects on a tile, and decreases value of hp and mana (properly for enemy and elemental), commands here are very temp.
    //private void performCombat()
    //{
    //    if(mapObjects[0] is Goblin && mapObjects[1].name == "Storm Elemental")
    //    {
    //        Destroy(mapObjects[0].gameObject);
    //    }
    //    else if(mapObjects[1] is Goblin && mapObjects[0].name == "Storm Elemental")
    //    {
    //        Destroy(mapObjects[1].gameObject);
    //    }
    //}
}
