﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//at end it should not inherit MonoBehaviour, but behave more like hex for serialization, but will do for now
public class MapObject : MonoBehaviour
{
    public enum FACTION_ID { PLAYER, ENEMY, NEUTRAL }
    public FACTION_ID factionID;
    public OBJECT_TYPE objectType;
    public string nameObject;
    public int healthMax;
    public int currentHealth { get; protected set; }
    public float damageTakenReduction;
    public Hex hex { get; set; }
    public HexComponent hexComponent { get; set; }
    public HexFinder hexFinder;
    public int spawnProbability;
    public int ID = 0;
    public bool canFly;
    public bool canSwim;
    public bool canWalk;
    public bool transformsOnDeath = false;
    public bool isClicked = false;

    public Transform AbilityVFXPoint;
    public FloatingText floatingText;
    public virtual void Awake()
    {
        //GetHexBelow();
        if (UnitManager.Instance == null)
        {
            Debug.LogWarning("Unit manager is still null!");
        }
        else
        {

            ID = UnitManager.Instance.GetNewID();
            gameObject.name = nameObject + " " + ID.ToString();
        }
        hexFinder = GetComponentInChildren<HexFinder>();
        floatingText = GetComponentInChildren<FloatingText>();
        currentHealth = healthMax;
    }

    protected virtual void Start()
    {
        GetHexBelow();
        if (hexComponent != null)
            hexComponent.AddObjectInfo(this);
    }

    virtual public void SetHex(Hex newHex)
    {
        hex = newHex;
    }

    public virtual void PayForAbility(int cost)
    {
        currentHealth -= cost;
        if (cost != 0)
            floatingText.ShowDamageReceived(cost);
        if (currentHealth > healthMax)
        {
            currentHealth = healthMax;
        }
        if (currentHealth <= 0)
        {
            Debug.Log(nameObject + " was destroyed after taking enough damage!");
            UIManager.Instance.AddBattleLog(nameObject + " was destroyed after taking enough damage!");
            Die();
        }
    }

    public virtual void Damage(int amount)
    {
        int proAmount;
        if (amount > 0)
        {
            proAmount = (int)(damageTakenReduction > 0.0f ? amount * (1 - damageTakenReduction) + 1 : amount);
        }
        else
        {
            proAmount = amount;
        }
        currentHealth -= proAmount;
        if (GetComponent<UnitController>() != null)
            floatingText.ShowDamageReceived(proAmount);
        damageTakenReduction = 0.0f;
        Debug.Log(nameObject + " took " + proAmount + " damage!");
        if (proAmount > 0)
            UIManager.Instance.AddBattleLog(nameObject + " took " + proAmount + " damage!");
        else if (proAmount < 0)
            UIManager.Instance.AddBattleLog(nameObject + " recovered " + -proAmount + " health!");
        if (currentHealth > healthMax)
        {
            currentHealth = healthMax;
        }
        if (currentHealth <= 0)
        {
            Debug.Log(nameObject + " was destroyed after taking enough damage!");
            UIManager.Instance.AddBattleLog(nameObject + " was destroyed after taking enough damage!");
            Die();
        }
    }

    public virtual void Die()
    {
        //ugly but should work
        /*
        if(this is Elemental)
            hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.Seed));
        */
        hexComponent.ClearObjectInfo(this);
        Destroy(gameObject);
    }

    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    public void DamageShake()
    {
        transform.DOShakePosition(0.8f, 0.1f);
    }

    public List<HexComponent> FilterMovementHexes(List<HexComponent> availableHexes)
    {
        List<HexComponent> hexes = new List<HexComponent>();
        foreach (HexComponent hex in availableHexes)
        {
            if (IsHexAllowed(hex.hex))
            {
                hexes.Add(hex);
            }

        }
        return hexes;
    }

    public bool IsHexAllowed(Hex h)
    {
        if (canFly == h.isFlyable)
            return true;
        if (h.objectOnHex != OBJECT_TYPE.None)
            return false;
        if (canSwim == h.isSwimmable)
            return true;
        if (canWalk == h.isWalkable)
            return true;
        // if (!((canFly && h.hex.isFlyable) || (canWalk && h.hex.isWalkable) || (canSwim && h.hex.isSwimmable)))
        //     return false;
        return false;
    }

    public bool CanMoveThrough(Hex h)
    {
        if (canFly)
            return true;
        return IsHexAllowed(h);
    }
    // a bit ugly but should work for now
    /// <summary>
    /// Gets the hex that is directly below unit via it's collider
    /// </summary>
    public void GetHexBelow()
    {
        if (hexFinder != null)
        {
            if (hexFinder.currentHex != null)
            {
                //Debug.Log("GetHex called by " + gameObject.name);
                try
                {
                    hexComponent.ClearObjectInfo(this);
                    hexComponent = hexFinder.currentHex;
                    if (!(hexComponent.mapObject == null))
                    {
                        hexComponent.mapObject = this;
                    }
                    hexComponent.AddObjectInfo(this);
                    hex = hexComponent.hex;
                    hex.hasObject = true;
                    hex.objectOnHex = objectType;
                }
                catch (NullReferenceException e)
                {
                    Debug.Log("No hex found");
                }

            }

        }
    }


}
