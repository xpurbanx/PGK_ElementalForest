﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SaveSystem 
{
    public static void SaveMap(MapData mapData)
    {
        string jsonMapData = JsonUtility.ToJson(mapData, true);
        File.WriteAllText(("Assets/Maps/" + mapData.mapName + ".json"), jsonMapData);
    }

    public static MapData LoadMap(string mapName)
    {
        string savePath = "Assets/Maps/" + mapName + ".json";
        if (File.Exists(savePath))
        {
            string json = File.ReadAllText(savePath);
            return JsonUtility.FromJson<MapData>(json);
        }
        else
        {
            Debug.LogWarning("Save file doesn't exist.");
            return null;
        }
    }

       
}
