﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PManaRegenAoE : Ability
{
    public int manaPerTurn;

    public override void Use()
    {

        List<HexComponent> hexes = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexes)
        {
            //if (hex.mapObject != null && hex.mapObject is Seed)
            if (hex.mapObject != null && hex.hex.objectOnHex == OBJECT_TYPE.Seed)
            {

                Seed seed = hex.mapObject.GetComponent<Seed>();
                seed.ChangeMana(manaPerTurn);
                if (abilityVFX != null)
                {
                    abilityVFX.Initialize(seed.AbilityVFXPoint.gameObject, owner.AbilityVFXPoint.gameObject);
                }
                PlayVFXTargetTween();
            }
        }
    }

}
