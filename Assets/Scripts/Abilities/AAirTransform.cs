﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AAirTransform : Ability
{
    public override void Use()
    {
        if (owner is Seed && owner.CanAct() && HasResources())
        {
            owner.hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.AirElemental));
            Click.Instance.DeselectObject();
            UIManager.Instance.UnitUIOff();
            LoadAndPlay(abilityClip);
            Destroy(owner.gameObject);
        }

    }

}
