﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ASpawnUnitHex : Ability
{
    public OBJECT_TYPE unitToSpawn;
    [HideInInspector]
    public TargetHex targeting;
    HexComponent targetHex;
    GameObject prefab;
    private void Start()
    {
        targeting = gameObject.AddComponent<TargetHex>();
        targeting.Initialize(this);
        targeting.needsEmptySpace = true;
    }

    public override bool CanBeUsed()
    {
        if (!HasResources())
            return false;
        else if (unitToSpawn == OBJECT_TYPE.Seed && UnitManager.Instance.elementalCount >= UnitManager.Instance.elementalLimit)
            return false;
        else if (unitToSpawn == OBJECT_TYPE.Treant && UnitManager.Instance.treantCount >= UnitManager.Instance.treantLimit)
            return false;
        return true;
    }

    public override void Use()
    {
        //Debug.Log("Ability use called");
        if (unitToSpawn == OBJECT_TYPE.Seed)
        {
            MainTree tree = null;
            if (owner is MainTree)
                tree = owner.GetComponent<MainTree>();
            if (UnitManager.Instance.elementalCount >= UnitManager.Instance.elementalLimit)
            {
                return;
            }
        }

        if (unitToSpawn == OBJECT_TYPE.Treant)
        {
            MainTree tree = null;
            if (owner is MainTree)
                tree = owner.GetComponent<MainTree>();
            if (UnitManager.Instance.treantCount >= UnitManager.Instance.treantLimit)
            {
                return;
            }
        }


        if (owner.CanAct())
            targeting.GetTarget(Act);
    }

    public override string FindCause()
    {
        if (unitToSpawn == OBJECT_TYPE.Seed && UnitManager.Instance.elementalCount >= UnitManager.Instance.elementalLimit)
        {
            return "Crystal limit reached";
        }
        if (unitToSpawn == OBJECT_TYPE.Treant && UnitManager.Instance.treantCount >= UnitManager.Instance.treantLimit)
        {
            return "Treant limit reached";
        }

        return "";
    }

    public void Act(HexComponent target)
    {
        //Debug.Log("Ability act called");
        if (target == null)
            return;
        prefab = MapObjectPrefabs.Instance.GetObjectPrefab(unitToSpawn);
        MapObject mapObject = prefab.GetComponent<MapObject>();
        if (target.hex.objectOnHex == OBJECT_TYPE.None && mapObject != null && IsTerrainCorrect(target.hex) && AreEffectsCorrect(target.hex))
        {
            target.SpawnObject(prefab);
            owner.ActionPerformed();
            LoadAndPlay(abilityClip);
        }
        else
        {
            //owner.ActionRestored();
        }
    }





}
