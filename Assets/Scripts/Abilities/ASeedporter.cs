﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASeedporter : Ability
{
    public bool seedPicked = false;
    [HideInInspector]
    public TargetHex targetingSpace;
    [HideInInspector]
    public TargetObject targetingSeed;
    public GameObject seedIndicator;
    int crystalManaLevel = 0;
    private void Start()
    {
        range = 3;
        targetingSpace = gameObject.AddComponent<TargetHex>();
        targetingSpace.Initialize(this);
        targetingSpace.needsEmptySpace = true;
        targetingSpace.needsOccupiedSpace = false;

        range = 2;
        targetingSeed = gameObject.AddComponent<TargetObject>();
        targetingSeed.Initialize(this);
        targetingSeed.needsOccupiedSpace = true;
        targetingSeed.needsEmptySpace = false;
        targetingSeed.requiredTypes.Add(OBJECT_TYPE.Seed);
    }

    public override void Use()
    {
        if (!seedPicked)
        {
            targetingSeed.GetTarget(ActPick);
            //targetingSpace.needsEmptySpace = false;
           // targetingSpace.needsOccupiedSpace = true;
        }
        else
        {
            targetingSpace.GetTarget(ActPlace);
           // targetingSpace.needsEmptySpace = true;
           // targetingSpace.needsOccupiedSpace = false;
        }
       // range = (seedPicked ? 3 : 2);
       // targetingSpace.Initialize(this);
       // targetingSpace.GetTarget(Act);
    }

    void Act(HexComponent target)
    {
        if(!seedPicked)
        {

           // ActPick(target);
        }
        else
        {

            ActPlace(target);
        }
        //LoadAndPlay(abilityClip);
    }
    
    void ActPick(MapObject target)
    {
        if (target == null)
            Debug.Log("Target pick is null!");
        Debug.Log(target.name + " was selected for pick");
        if (target.objectType == OBJECT_TYPE.Seed && owner.CanAct())
        {
            Seed crystal = (Seed)target;
            if (seedIndicator != null)
                seedIndicator.SetActive(true);
            seedPicked = true;
            crystalManaLevel = crystal.manaCurrent;
            crystal.Transported();
            owner.ActionPerformed();
            UnitManager.Instance.elementalCount++;
            LoadAndPlay(abilityClip);
        }
        else
        {
            //owner.ActionRestored();
        }
    }

    void ActPlace(HexComponent target)
    {
        //owner.actionDone = true;
        if (target != null && owner.CanAct())
        {
            if (seedIndicator != null)
                seedIndicator.SetActive(false);
            target.SpawnCrystal(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.Seed), crystalManaLevel);
            seedPicked = false;
            owner.ActionPerformed();
            UnitManager.Instance.elementalCount--;
            LoadAndPlay(abilityClip);
        }
        else
        {
            //owner.ActionRestored();
        }
    }


}
