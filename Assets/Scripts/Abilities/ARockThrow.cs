﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARockThrow : Ability
{
    private int damageAmount;
    [HideInInspector]
    public TargetObject targetingObject;
    [HideInInspector]
    public TargetHex targetingHex;
    public HexComponent startingHex;
    public bool startsFire = false;
    public bool stopsFire = false;
    public bool noMapFeatureDamage = false;
    bool hasRock = false;
    MapFeature targetedRock;

    public int pickupRange = 1;
    public int throwRange = 3;

    private void Start()
    {
        targetingObject = gameObject.AddComponent<TargetObject>();
        range = pickupRange;
        targetingObject.Initialize(this);
        targetingObject.needsOccupiedSpace = true;
        targetingObject.requiredTypes.Add(OBJECT_TYPE.Rock);

        targetingHex = gameObject.AddComponent<TargetHex>();
        range = throwRange;
        targetingHex.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
        {
            targetingObject.GetTarget(ActObject);

        }
    }
    public void ActObject(MapObject target)
    {
        if (target == null || !(target is MapFeature))
        {
            return;
        }
        else
        {
            if (target.objectType == OBJECT_TYPE.Rock)
            {
                targetedRock = (MapFeature)target;
                damageAmount = (targetedRock.healthMax / 5);
                //owner.ActionPerformed();
                owner.transform.DOLookAt(target.transform.position, 0.2f);
                startingHex = targetedRock.hexComponent;
                targetingHex.GetTarget(ActHex);
                //SpendResources();      
            }
        }

    }

    public void ActHex(HexComponent target)
    {
        MapObject objectOnHex = null;
        if (target == null && targetedRock == null)
        {
            owner.ActionRestored();
            return;
        }
        else
        {
            owner.transform.DOLookAt(target.transform.position, 0.2f);
            SpendResources();
            objectOnHex = target.mapObject;
            startingHex.ClearObjectInfo(targetedRock);
            //AbilityVFX.ThrowObject(targetedRock.gameObject, target.transform.position);
            Vector3 endPos = target.transform.position;
            if (objectOnHex != null)
            {
                objectOnHex.Damage(damageAmount);
                objectOnHex.DamageShake();
                targetedRock.Damage(damageAmount);
            }
           // startingHex.ClearObjectInfo(targetedRock);
            targetedRock.transform.DOJump(endPos, 1f, 1, 1f).OnComplete(() =>
            {

                if (target.mapObject == null)
                {
                   // target.ClearObjectInfo(targetedRock);
                    targetedRock.hexComponent = target;
                    targetedRock.Damage(damageAmount);
                    if (target.mapFeature != null && target.mapFeature.featureType == FEATURE_TYPE.LIGHTFOREST)
                    {
                        target.mapFeature.Die();
                    }
                    target.AddObjectInfo((MapFeature)targetedRock);
                    target.mapFeature = targetedRock;
                    target.mapFeature.featureType = FEATURE_TYPE.ROCK;
                }

                else
                {
                    targetedRock.Die();
                }


                if (target.hex.TerrainType == TERRAIN_TYPE.WATER)
                {
                    target.ClearObjectInfo(targetedRock);
                    targetedRock.Die();
                }

            }
            );
            //there is unit on targeted hex

            //unit is dead after last throw, place rock on it it's spot
            //else if (target.mapObject != null && (objectOnHex.GetCurrentHealth() <= 0 && objectOnHex.transformsOnDeath == false))
            //{
            //    target.ClearObjectInfo(objectOnHex);
            //    target.AddObjectInfo(targetedRock);
            //    //targetedRock.Damage(damageAmount);
            //}
            ////bounces off target if not dead and lands on random hex nearby if empty
            //else
            //{
            //    List<HexComponent> neighbours = HexAccessor.GetNeighbourHexComponents(target);
            //    neighbours = targetedRock.FilterMovementHexes(neighbours);
            //    neighbours = HexAccessor.CheckForObjects(neighbours);
            //    neighbours = HexAccessor.CheckForFeatures(neighbours);
            //    if (neighbours.Count > 0)
            //    {
            //        //target.ClearObjectInfo(targetedRock);
            //        int selectedNeighbour = Random.Range(0, neighbours.Count);
            //        neighbours[selectedNeighbour].ClearObjectInfo(targetedRock);
            //        targetedRock.transform.DOJump(neighbours[selectedNeighbour].transform.position, 0.6f, 1, 1f);
            //        neighbours[selectedNeighbour].AddObjectInfo(targetedRock);
            //        targetedRock.Damage(damageAmount);


            //        //neighbours[selectedNeighbour].transform.DOJump(target.transform.position, 1f, 1, 1f)
            //    }
            //    else
            //    {
            //        targetedRock.Die();
            //    }
            //targetedRock.Die();
            //target.AddObjectInfo(targetedRock);
            //}

            ////there is unit on targeted hex
            //if (target.mapObject != null)
            //{
            //    objectOnHex.Damage(damageAmount);
            //    objectOnHex.DamageShake();
            //}
            ////unit is dead after last throw
            //if ((objectOnHex == null || objectOnHex.GetCurrentHealth() <= objectOnHex.healthMax) && objectOnHex.transformsOnDeath == false)
            //{
            //    objectOnHex.hexComponent.ClearObjectInfo(objectOnHex);
            //    target.AddObjectInfo(targetedRock);
            //    targetedRock.Damage(damageAmount);
            //}
            //else
            //{
            //    targetedRock.Die();
            //    //target.AddObjectInfo(targetedRock);
            //}
            owner.ActionPerformed();
            LoadAndPlay(abilityClip);
            //PlayVFXTargetTween();

        }

    }
}
