﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AFireTransform : Ability
{

    public override bool CanBeUsed()
    {
        if (!HasResources())
            return false;
        else if (!(IsTerrainCorrect(owner.hex) && AreEffectsCorrect(owner.hex)))
            return false;
        return true;
    }

    public override string FindCause()
    {
        Seed seed = (Seed)owner;
        if(!AreEffectsCorrect(owner.hex))
        {
            return "Requires a fire";
        }
        else if (!IsTerrainCorrect(owner.hex))
        {
            return "Wrong terrain type";
        }
        else if (!HasResources())
        {
            return "Not enough mana";
        }

        return "";
    }
    public override void Use()
    {
        if (owner is Seed && owner.CanAct() && HasResources())
        {
            if (IsTerrainCorrect(owner.hex) && AreEffectsCorrect(owner.hex))
            {
                owner.hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.FireElemental));
                Click.Instance.DeselectObject();
                UIManager.Instance.UnitUIOff();
                LoadAndPlay(abilityClip);
                Destroy(owner.gameObject);
            }
            else
            {
                UIManager.Instance.promptText.text = "Seed must be placed on burning hex!";
            }
        }
    }

}
