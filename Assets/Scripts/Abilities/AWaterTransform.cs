﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AWaterTransform : Ability
{
    public override bool CanBeUsed()
    {
        if (!HasResources())
            return false;
        else if (!IsTerrainCorrect(owner.hex))
            return false;
        return true;
    }

    public override string FindCause()
    {
        Seed seed = (Seed)owner;
        if(!IsTerrainCorrect(owner.hex))
        {
            return "Wrong terrain type";
        }
        else if (!HasResources())
        {
            return "Not enough mana";
        }

        return "";
    }
    public override void Use()
    {
        if (owner is Seed && owner.CanAct() && HasResources())
        {
            if (IsTerrainCorrect(owner.hex))
            {
                owner.hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.WaterElemental));
                Click.Instance.DeselectObject();
                UIManager.Instance.UnitUIOff();
                LoadAndPlay(abilityClip);
                Destroy(owner.gameObject);
            }
            else
            {
                UIManager.Instance.promptText.text = "Seed must be placed on water!";
            }
        }
    }
}
