﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AFuse : Ability
{
    TargetObject targeting;
    private void Start()
    {
        targeting = gameObject.AddComponent<TargetObject>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct())
            targeting.GetTarget(Act);
    }

    void Act(MapObject target)
    {
        if (target is Elemental)
        {
            // Elemental target = targetUnit.GetComponent<Elemental>();
            // Elemental owner = owner.GetComponent<Elemental>();
            owner.actionDone = true;
            HexComponent spawnHex;
            if (owner.objectType == OBJECT_TYPE.AirElemental && target.objectType == OBJECT_TYPE.WaterElemental)
            {
                spawnHex = target.hexComponent;
                Click.Instance.DeselectObject();
                UIManager.Instance.UnitUIOff();
                owner.hexComponent.ClearObjectInfo(owner);
                Destroy(owner.gameObject);
                Destroy(target.gameObject);
                spawnHex.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.StormElemental));
                LoadAndPlay(abilityClip);
            }
            else if (owner.objectType == OBJECT_TYPE.WaterElemental && target.objectType == OBJECT_TYPE.AirElemental)
            {
                spawnHex = owner.hexComponent;
                Click.Instance.DeselectObject();
                UIManager.Instance.UnitUIOff();
                target.hexComponent.ClearObjectInfo(target);
                Destroy(target.gameObject);
                Destroy(owner.gameObject);
                spawnHex.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.StormElemental));
                LoadAndPlay(abilityClip);
            }
            else // if no fuse was available, then restore action
                owner.ActionRestored();

            //for magma elemental
            //if (owner.objectType == OBJECT_TYPE.EarthElemental && target.objectType == OBJECT_TYPE.FireElemental)
            //{
            //    spawnHex = target.hexComponent;
            //    Click.Instance.DeselectObject();
            //    UIManager.Instance.UnitUIOff();
            //    Destroy(owner.gameObject);
            //    Destroy(target.gameObject);
            //    spawnHex.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.StormElemental));
            //}


        }
    }

    
}
