﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AZeustrike : Ability
{
    public int damage;
    public int[] ownRanges = {0,0,0};
    //public int[] ownDamages = {0,0,0};
    private int terrainIndex;
    [HideInInspector]
    public TargetHex targeting;

    private void Start()
    {
        targeting = gameObject.AddComponent<TargetHex>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
            targeting.GetTarget(Act);
    }

    void Act(HexComponent target)
    {

        if (target != null)
        {
            owner.ActionPerformed();
            switch (target.hex.TerrainType)
            {
                case TERRAIN_TYPE.WATER: { terrainIndex = 2; break; }
                case TERRAIN_TYPE.GRASS: { terrainIndex = 1; break; }
                case TERRAIN_TYPE.EARTH: { terrainIndex = 0; break; }
            }

            List<HexComponent> hexes = HexAccessor.GetHexesAround(target, ownRanges[terrainIndex]);
            foreach (HexComponent hex in hexes)
            {
                if (hex.mapObject != null && hex.mapObject is Unit)
                {
                    Unit unit = hex.mapObject.GetComponent<Unit>();
                    if (target != owner)
                    {
                        unit.Damage(calcDamage(terrainIndex));
                    }
                }
            }
            SpendResources();
            LoadAndPlay(abilityClip);

        }
        else
        {
            //owner.ActionRestored();
        }
    }

    private int calcDamage(int param)
    {
        int newDamage = damage / 2;
        for (int i = 0; i < param; i++)
        {
            newDamage *= 2;
        }
        return newDamage;
    }
}
