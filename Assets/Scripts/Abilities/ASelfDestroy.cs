﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASelfDestroy : Ability
{
    public override void Use()
    {
        Initialize();
        if (owner is Elemental)
        {
            //updateSeedCount();
            //if (seedLimit > seedUsed)
            {
                //updateSeedCount();
                //owner.hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.Seed));
                LoadAndPlay(abilityClip);
                Click.Instance.DeselectObject();
                UIManager.Instance.UnitUIOff();
                //Destroy(owner.gameObject);
                owner.Die();
            }
            /*
            else
            {
                Debug.Log(seedUsed.ToString() + "in use. Cant use more seeds");
            }
            */
        }
    }

}
