﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelf : TargetSystem<Unit>
{
    public override Unit Target()
    {
        return owner;
    }
    public override void GetTarget(Action<Unit> callback)
    {
        StopAllCoroutines();
        StartCoroutine(WaitForTarget(callback));
    }
    public override IEnumerator WaitForTarget(Action<Unit> callback)
    {
        callback(Target());
        yield return null;
        StopAllCoroutines();
    }

}
