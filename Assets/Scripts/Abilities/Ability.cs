﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ABILITY_TARGET { HEX, OBJECT, SELF }
public abstract class Ability : MonoBehaviour
{
    public string abilityName;
    public int healthCost = 0;
    public int range;
    public int maxCooldown;
    public int cooldownLeft;
    public bool targetFriendly;
    public bool isPassive;
    public AudioClip abilityClip;
    public ABILITY_TARGET targetType; // used only for icons
    public List<TERRAIN_TYPE> allowedTerrains = new List<TERRAIN_TYPE>();
    public List<EFFECT_TYPE> requiredEffects = new List<EFFECT_TYPE>();
    public List<OBJECT_TYPE> excludedObjects = new List<OBJECT_TYPE>();
    [HideInInspector]
    public Unit owner;
    public AbilityVFX abilityVFX;
    [TextArea(15, 20)]
    public string description;
    private void OnEnable()
    {
        owner = GetComponentInParent<Unit>();
    }

    /// <summary>
    /// Used when clicking. 
    /// </summary>
    public abstract void Use();

    /// <summary>
    /// Checks if unit has health or mana to perform ability
    /// </summary>
    /// <returns>True has resources, false if not</returns>
    protected bool HasResources()
    {
        if (owner.manaUser)
        {
            Seed seed = (Seed)owner;
            if (seed.manaCurrent >= healthCost)
                return true;
            else
                return false;
        }
        else
        {

            if (owner.currentHealth >= healthCost && cooldownLeft == 0)
                return true;
            else
                return false;
        }

    }

    public virtual bool CanBeUsed()
    {
        if (!HasResources())
            return false;
        return true;
    }

    public virtual string FindCause()
    {
        if(owner.manaUser)
        {
            if (!HasResources())
            {
                return "Not enough mana";
            }
        }


        return "";
    }

    /// <summary>
    /// Sets up owner and other things
    /// </summary>
    protected void Initialize()
    {
        owner = GetComponentInParent<Unit>();
    }

    /// <summary>
    /// Spends resources for ability usage
    /// </summary>
    protected void SpendResources()
    {
        if (owner.manaUser == true)
        {
            Seed seed = (Seed)owner;
            seed.ChangeMana(-healthCost);
            setCooldown();
        }
        else
        {
            owner.PayForAbility(healthCost);

        }
        UIManager.Instance.UpdateUnitUIInfo(owner);
    }

    /// <summary>
    /// Sets cooldown after ability usage
    /// </summary>
    protected void setCooldown()
    {
        if (maxCooldown > 0) { cooldownLeft = maxCooldown; }
        else { cooldownLeft = 0; }
    }

    /// <summary>
    /// Checks if hex terrain type is the same as required type
    /// </summary>
    /// <param name="hex">Hex to check for terrain</param>
    /// <returns>True if terrain matches, false if not</returns>
    protected bool IsTerrainCorrect(Hex hex)
    {
        if (!allowedTerrains.Any())
            return true;
        if (allowedTerrains.Contains(hex.TerrainType))
            return true;
        else
            return false;
    }

    /// <summary>
    /// Checks if hex terrain type is the same as required type
    /// </summary>
    /// <param name="hex">Hex to check for terrain</param>
    /// <returns>True if terrain matches, false if not</returns>
    protected bool IsObjectCorrect(MapObject mapObject)
    {
        if (!excludedObjects.Any())
            return true;
        if (excludedObjects.Contains(mapObject.objectType))
            return false;
        else
            return true;
    }



    /// <summary>
    /// Checks if hex has all required effects
    /// </summary>
    /// <param name="hex">Hex to check for effects</param>
    /// <returns>True if contains all effects, false if doens't contain all of them.</returns>
    protected bool AreEffectsCorrect(Hex hex)
    {
        if (!requiredEffects.Any())
            return true;

        foreach (EFFECT_TYPE effect in requiredEffects)
        {
            if (hex.effects.Contains(effect))
            {
                //if contains, then check other required effect on hex
                continue;
            }
            else
                //if doesn't contain, then return false
                return false;
        }
        //returns true, because contains all required effects
        return true;
    }


    /// <summary>
    /// Sets allowed terrains depending on object movement permissions
    /// </summary>
    /// <param name="mapObject">MapObject which we check movement permissons</param>
    protected void MovementBasedTerrains(MapObject mapObject)
    {
        if (mapObject.canFly)
        {
            //if list is null, then all terrains are allowed
            allowedTerrains.Clear();
            return;
        }
        if (mapObject.canWalk)
        {
            allowedTerrains.Add(TERRAIN_TYPE.BARREN);
            allowedTerrains.Add(TERRAIN_TYPE.EARTH);
            allowedTerrains.Add(TERRAIN_TYPE.GRASS);
        }
        if (mapObject.canSwim)
        {
            allowedTerrains.Add(TERRAIN_TYPE.WATER);
        }

    }

    public void PlayVFXTarget()
    {
        if (abilityVFX != null)
        {
            abilityVFX.PlayOnTarget();

        }
    }

    public void PlayVFXTargetTween()
    {
        if (abilityVFX != null)
        {
            abilityVFX.PlayTweenTarget();

        }
    }

    public void PlayVFXSelf()
    {
        if (abilityVFX != null)
        {
            abilityVFX.PlayOnSelf();

        }
    }

    public void LoadAndPlay(AudioClip clip)
    {
        if (clip != null)
        {
            AudioManager.Instance.LoadSound(clip);
            AudioManager.Instance.PlaySound();
        }
    }

}
