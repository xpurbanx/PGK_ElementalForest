﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASeedPlace : Ability
{
    [HideInInspector]
    public TargetHex targeting;
    private void Start()
    {
        targeting = gameObject.AddComponent<TargetHex>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        ASeedPickup skill = owner.abilityList.LookForAbility<ASeedPickup>();
        if (skill != null && skill.seedPicked == true && owner.CanAct())
        {
            targeting.GetTarget(Act);
        }
        else
        {
            UIManager.Instance.promptText.text = "You have not picked seed!";
        }
    }

    public override string FindCause()
    {
        if (UnitManager.Instance.elementalLimit <= UnitManager.Instance.elementalCount)
        {
            return "Crystal limit reached";
        }

        return "";
    }

    void Act(HexComponent target)
    {
        owner.actionDone = true;
        if (target != null)
        {
            target.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.Seed));

        }
        else
        {
            owner.ActionRestored();
        }
    }

}
