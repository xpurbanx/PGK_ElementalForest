﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PHealthRegenAoE : Ability
{
    public int healthPerTurn;

    public override void Use()
    {
        List<HexComponent> hexes = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexes)
        {
            if (hex.mapObject != null && hex.mapObject is Unit && AreEffectsCorrect(hex.hex))
            {
                Unit unit = hex.mapObject.GetComponent<Unit>();
                if (unit.factionID == owner.factionID)
                {
                    unit.Damage(-healthPerTurn);
                }
            }
        }
    }

}
