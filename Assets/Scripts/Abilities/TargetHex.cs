﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetHex : TargetSystem<HexComponent>
{
    public override HexComponent Target()
    {
        return target;
    }

    public override void GetTarget(Action<HexComponent> callback)
    {
        StopAllCoroutines();
        StartCoroutine(WaitForTarget(callback));
    }

    public override IEnumerator WaitForTarget(Action<HexComponent> callback)
    {
        ShowRange();
        target = null;
        GameManager.Instance.TargetHexSelect(target);
        while (Click.Instance.targetedHex == null)
        {
            yield return null;
        }
        target = Click.Instance.targetedHex;

        Click.Instance.ChangeClickMode(Click.CLICK_MODE.All);
        if (!IsInRange(target.hex) || target == owner.hexComponent)
        {
            callback(null);
            HideRange();
        }
        else //uses ability as intended
        {
            callback(target);
            HideRange();

        }
        StopAllCoroutines();
    }
}
