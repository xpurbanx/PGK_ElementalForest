﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASelfGuard : Ability
{
    public float reduction;
    [HideInInspector]
    public TargetSelf targeting;

    private void Start()
    {
        targeting = gameObject.AddComponent<TargetSelf>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
        {
            targeting.GetTarget(Act);

        }
    }

    public void Act(Unit target)
    {
        if(target.damageTakenReduction == 0.0f)
        {
            SpendResources();
            owner.ActionPerformed();
            LoadAndPlay(abilityClip);
            target.damageTakenReduction = reduction;
            Debug.Log("Object increased defense!");
        }

    }
}
