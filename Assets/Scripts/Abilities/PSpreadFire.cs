﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PSpreadFire : Ability
{
    public override void Use()
    {
        List<HexComponent> hexes = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexes)
        {
            hex.StartFire();
        }
    }
}
