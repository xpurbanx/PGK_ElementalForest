﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetObject : TargetSystem<MapObject>
{
    public override MapObject Target()
    {
        return target;
    }

    public override void GetTarget(Action<MapObject> callback)
    {
        StopAllCoroutines();
        StartCoroutine(WaitForTarget(callback));
    }
    public override IEnumerator WaitForTarget(Action<MapObject> callback)
    {
        ShowRange();
        target = null;
        GameManager.Instance.TargetObjectSelect(target);
        while (Click.Instance.targetedObject == null)
        {
            yield return null;
        }
        target = Click.Instance.targetedObject;
        Click.Instance.ChangeClickMode(Click.CLICK_MODE.All);
        if ((!IsInRange(target.hexComponent.hex) || target == owner || (targetFriendly == false && target.factionID == owner.factionID)))
        {
            HideRange();
            callback(null);
        }
        else
        {
            HideRange();
            callback(target);
        }
        StopAllCoroutines();
    }
}
