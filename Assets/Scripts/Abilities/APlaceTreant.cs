﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APlaceTreant : Ability
{
    public GameObject treantPrefab;
    [HideInInspector]
    public TargetHex targeting;
    HexComponent targetHex;


    public override void Use()
    {
        targeting.needsEmptySpace = true;
        targeting.WaitForTarget(Act);
        owner.actionDone = true;
    }

    public override string FindCause()
    {
        if (UnitManager.Instance.treantLimit <= UnitManager.Instance.treantCount)
        {
            return "Treant limit reached";
        }

        return "";
    }
    public void Act(HexComponent target)
    {
        if (targetHex.mapObject == null)
        {
            targetHex.SpawnObject(treantPrefab);
        }
        else
        {
            owner.ActionRestored();
        }
    }
}
