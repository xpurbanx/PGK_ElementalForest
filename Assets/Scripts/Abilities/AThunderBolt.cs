﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AThunderBolt : Ability
{
    public int damage;
    [HideInInspector]
    public TargetHex targeting;

    private void Start()
    {
        targeting = gameObject.AddComponent<TargetHex>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
            targeting.GetTarget(Act);
    }

    void Act(HexComponent target)
    {

        if (target != null)
        {
            if (abilityVFX != null)
            {
                abilityVFX.Initialize(target.gameObject, owner.AbilityVFXPoint.gameObject);
            }
            owner.ActionPerformed();
            if (target.hex.TerrainType == TERRAIN_TYPE.GRASS)
            {
                target.StartFire();
            }

            if (target.mapObject != null)
            {
                target.mapObject.Damage(damage);
            }
            SpendResources();
            LoadAndPlay(abilityClip);
            PlayVFXTargetTween();
        }
        else
        {
            //owner.ActionRestored();
        }
    }
}
