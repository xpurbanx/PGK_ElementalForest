﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASeedPickup : Ability
{
    public bool seedPicked = false;
    [HideInInspector]
    public TargetObject targeting;
    private void Start()
    {
        targeting = gameObject.AddComponent<TargetObject>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct())
            targeting.GetTarget(Act);
    }

    void Act(MapObject target)
    {
        if (target is Seed && seedPicked == false)
        {
            seedPicked = true;
            Destroy(target.gameObject);
            owner.ActionPerformed();
        }
        else
        {
            //owner.ActionRestored();
        }
    }
}
