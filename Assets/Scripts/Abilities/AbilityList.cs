﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityList : MonoBehaviour
{
    public List<Ability> abilities = new List<Ability>();

    private void OnEnable()
    {
        LoadUnitAbilities();
    }

    public void LoadUnitAbilities()
    {
        foreach(Ability ability in GetComponentsInChildren<Ability>())
        {
            if (!abilities.Contains(ability))
            {
                abilities.Add(ability);
            }
        }
    }

    /// <summary>
    /// Searches ability list for ability that matches given type and returns it
    /// </summary>
    /// <typeparam name="T">Searched ability class</typeparam>
    /// <returns>Returns ability from ability list</returns>
    public T LookForAbility<T>()
    {
        foreach (Ability ability in abilities)
        {
            if (ability is T)
            {
                return ability.GetComponent<T>();
            }
        }

        return default;
    }

}
