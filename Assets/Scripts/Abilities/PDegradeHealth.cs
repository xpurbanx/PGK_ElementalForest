﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PDegradeHealth : Ability
{
    public int healthLostAmount;
    public override void Use()
    {
        if (!AreEffectsCorrect(owner.hexComponent.hex))
        {
            owner.Damage(healthLostAmount);
        }
    }
}
