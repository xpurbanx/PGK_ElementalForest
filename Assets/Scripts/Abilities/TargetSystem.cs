﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TargetSystem<T> : MonoBehaviour
{
    protected Unit owner;
    protected int range;
    protected T target;
    protected bool targetFriendly;
    public bool needsEmptySpace;
    public bool needsOccupiedSpace;

    public List<OBJECT_TYPE> requiredTypes = new List<OBJECT_TYPE>();

    List<HexComponent> hexesInRange = new List<HexComponent>();

    /// <summary>
    /// Method for targeting, which doesn't need to select target by clicking
    /// </summary>
    /// <returns>Returns selected target</returns>
    public abstract T Target();

    /// <summary>
    /// Method for calling coroutine of target selection
    /// </summary>
    public abstract void GetTarget(Action<T> callback);

    /// <summary>
    /// Method for targeting, which needs target selection by click. As parametr give method to execute after target is found.
    /// </summary>
    /// <param name="callback">Method to perform after target is obtained</param>
    /// <returns></returns>
    /// 
    public abstract IEnumerator WaitForTarget(Action<T> callback);
    /// <summary>
    /// Gives system basic information needed for targeting
    /// </summary>
    /// <param name="ability">Ability that uses this target system</param>
    public void Initialize(Ability ability)
    {
        owner = ability.owner;
        range = ability.range;
        targetFriendly = ability.targetFriendly;
    }

    /// <summary>
    /// Checks whether target hex is in range
    /// </summary>
    /// <param name="target">Selected hex to check</param>
    /// <returns>Returns true if target hex is in range</returns>
    protected bool IsInRange(Hex target)
    {

        if (HexAccessor.Distance(GetComponentInParent<MapObject>().hex, target) <= range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Outlines hexes that are in range of ability
    /// </summary>
    public void ShowRange()
    {
        if (needsEmptySpace)
        {
            OutlineEmptyInRange();
        }
        else if (needsOccupiedSpace)
        {
            if (requiredTypes.Count == 0)
                OutlineMapObjectsInRange();
            else
                OutlineSpecificInRange();
        }
        else
            OutlineAllInRange();
    }

    /// <summary>
    /// Outlines hexes that are in range of ability
    /// </summary>
    public void OutlineAllInRange()
    {
        hexesInRange = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexesInRange)
        {
            hex.OutlineRange();
        }
    }

    /// <summary>
    /// Outlines hexes that are in range of ability where no objects occupy space
    /// </summary>
    public void OutlineEmptyInRange()
    {
        hexesInRange = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexesInRange)
        {
            if (hex.mapObject == null)
                hex.OutlineRange();
        }
    }

    /// <summary>
    /// Outlines hexes that are in range of ability where no objects occupy space
    /// </summary>
    public void OutlineMapObjectsInRange()
    {
        hexesInRange = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexesInRange)
        {
            if (hex.mapObject != null)
                hex.OutlineRange();
        }
    }

    /// <summary>
    /// Outlines hexes that are in range of ability where no objects occupy space
    /// </summary>
    public void OutlineSpecificInRange()
    {
        hexesInRange = HexAccessor.GetHexesAround(owner.hexComponent, range);
        foreach (HexComponent hex in hexesInRange)
        {
            if (hex.mapObject != null && requiredTypes.Contains(hex.mapObject.objectType))
                hex.OutlineRange();
        }
    }

    /// <summary>
    /// Hides previously outlined hexes
    /// </summary>
    public void HideRange()
    {
        hexesInRange = HexAccessor.GetHexesAround(owner.hexComponent, range);
        if (hexesInRange != null)
        {
            foreach (HexComponent hex in hexesInRange)
            {
                hex.isInRange = false;
                hex.OutlinePrevious();
            }

        }
    }
}
