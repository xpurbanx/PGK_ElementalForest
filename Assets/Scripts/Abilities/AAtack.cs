﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AAtack : Ability
{
    public int damageAmount;
    public int bonusDamageToMapfeature;
    [HideInInspector]
    public TargetObject targeting;
    [HideInInspector]
    public TargetHex targetingHex;
    public bool startsFire = false;
    public bool stopsFire = false;
    public bool noMapFeatureDamage = false;
    public bool canTargetTerrain = false;

    private void Start()
    {
        targeting = gameObject.AddComponent<TargetObject>();
        targeting.Initialize(this);
        targeting.needsOccupiedSpace = true;
        if (canTargetTerrain)
        {
            targetingHex = gameObject.AddComponent<TargetHex>();
            targetingHex.Initialize(this);
        }
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
        {
            if (canTargetTerrain)
            {
                targetingHex.GetTarget(Act);
            }
            else
            {
                targeting.GetTarget(Act);

            }

        }
    }

    public void Act(MapObject target)
    {

        if (target == null || target.factionID == owner.factionID)
        {
            return;
        }
        else
        {
            owner.ActionPerformed();
            owner.transform.DOLookAt(target.transform.position, 0.2f);
            SpendResources();
            if (abilityVFX != null)
            {
                abilityVFX.Initialize(target.AbilityVFXPoint.gameObject, owner.AbilityVFXPoint.gameObject);
            }
            if (startsFire)
            {
                target.hexComponent.StartFire();
            }
            if (stopsFire)
            {
                target.hexComponent.StopFire();
            }
            if (target is MapFeature && noMapFeatureDamage)
            {
                if (target.hexComponent.mapFeature.featureType == FEATURE_TYPE.FOREST || target.hexComponent.mapFeature.featureType == FEATURE_TYPE.LIGHTFOREST)
                    target.Damage(-damageAmount / 2);
            }
            else if (target is MapFeature)
            {
                target.Damage(damageAmount + bonusDamageToMapfeature);
            }
            else if (target != null)
            {
                target.Damage(damageAmount);
            }
            target.DamageShake();
            LoadAndPlay(abilityClip);
            owner.currentAnim = ANIMATION_TYPE.ATTACK;
            owner.OnAnimationNeeded();
            owner.lastActionComplete = true;
            PlayVFXTargetTween();

        }

    }

    public void Act(HexComponent target)
    {

        if (target == null)
        {
            return;
        }
        else
        {
            owner.ActionPerformed();
            owner.transform.DOLookAt(target.transform.position, 0.2f);
            SpendResources();
            if (abilityVFX != null)
            {
                abilityVFX.Initialize(target.gameObject, owner.AbilityVFXPoint.gameObject);
            }
            if (startsFire)
            {
                target.StartFire();
            }
            if (stopsFire)
            {
                target.StopFire();
            }
            if (target.mapFeature != null && noMapFeatureDamage)
            {
                if (target.mapFeature.featureType == FEATURE_TYPE.FOREST || target.mapFeature.featureType == FEATURE_TYPE.LIGHTFOREST)
                    target.mapFeature.Damage(-damageAmount / 2);
            }
            else if (target.mapFeature != null)
            {
                target.mapFeature.Damage(damageAmount + bonusDamageToMapfeature);
            }
            if (target.mapObject != null)
            {
                if (targetFriendly && target.mapObject.factionID == MapObject.FACTION_ID.PLAYER)
                {
                    if ((owner.objectType == OBJECT_TYPE.WaterElemental) && (target.mapObject.objectType == OBJECT_TYPE.FireElemental))
                        target.mapObject.Damage(damageAmount);

                }
                else if (target.mapObject.factionID == MapObject.FACTION_ID.ENEMY)
                {

                    target.mapObject.Damage(damageAmount);
                }
            }
            if (target.mapFeature != null)
                target.mapFeature.DamageShake();
            if (target.mapObject != null)
                target.mapObject.DamageShake();
            LoadAndPlay(abilityClip);
            owner.currentAnim = ANIMATION_TYPE.ATTACK;
            owner.OnAnimationNeeded();
            owner.lastActionComplete = true;
            PlayVFXTargetTween();

        }

    }
}
