﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStepBuff : Ability
{
    public int buffAmount;
    [HideInInspector]
    public TargetObject targeting;

    private void Start()
    {
        targeting = gameObject.AddComponent<TargetObject>();
        targeting.Initialize(this);
    }
    public override void Use()
    {
        if (owner.CanAct() && HasResources())
        {
            targeting.GetTarget(Act);

        }
    }

    public void Act(MapObject target)
    {
        Unit unit = (Unit) target;
        if (unit == null)
        {
            return;
        }
        if (IsObjectCorrect(unit))
        {
            if (unit.factionID == owner.factionID)
            {
                owner.ActionPerformed();
                SpendResources();
                unit.UpdateMovement(buffAmount);
            }
            else
            {
                owner.ActionPerformed();
                SpendResources();
                unit.UpdateMovement(-buffAmount);
            }
        }
        


    }
}
