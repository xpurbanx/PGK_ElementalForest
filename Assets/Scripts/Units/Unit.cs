﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Base class for all in-game units
[RequireComponent(typeof(UnitController))]
[RequireComponent(typeof(AbilityList))]
public class Unit : MapObject
{
    private Vector3 goalPosition;//
    // The amount of tiles the unit can move each turn
    public bool turnEnded = false;
    public bool actionDone = false;
    public int movementBase;
    public bool isMoving = false;
    public bool isMovementFinished = true;
    public bool manaUser = false;
    [HideInInspector]
    public int movementLeft;
    public UnitController controller;
    public string description;
    public int manaMax;
    public int manaCurrent = 0;
    public GameObject actionIndicator;
    public AbilityList abilityList;
    public bool lastActionComplete = true;
    //public int manaMax = 0;
    [HideInInspector]
    //public int manaCurrent;
    // The speed of movement towards tile
    float speed = 0.1f;
    public event EventHandler StatusChanged;
    public event EventHandler AbilityUsed;
    public ANIMATION_TYPE currentAnim;
    protected virtual void Awake()
    {
        base.Awake();
        movementLeft = movementBase;
        controller = GetComponent<UnitController>();
        abilityList = GetComponent<AbilityList>();

    }

    protected override void Start()
    {
        base.Start();
        //description = "This is a unit";
        //GetHexBelow();
        if (factionID == FACTION_ID.PLAYER)
        {
            UnitManager.Instance.AddPlayerUnit(this);
        }
        else if (factionID == FACTION_ID.ENEMY)
        {
            UnitManager.Instance.AddEnemyUnit(this);
        }
        //manaCurrent = manaMax;
        //just so main tree has action at start
        if (objectType != OBJECT_TYPE.MainTree)
            EndTurn();
        // hexFinder.VerifyHexComponent();
    }

    // Moves the unit towards its destination
    public override void Damage(int amount)
    {
        base.Damage(amount);
        currentAnim = ANIMATION_TYPE.DAMAGED;
        OnAnimationNeeded();

    }

    public bool CanAct()
    {
        return !actionDone;
    }
    public void ActionPerformed()
    {
        actionDone = true;
        if (controller.currentlyIsSelected)
            UIManager.Instance.UpdateUnitUIInfo(this);
        actionIndicator.gameObject.SetActive(false);
    }

    public void ActionRestored()
    {
        actionDone = false;
        if (controller.currentlyIsSelected)
            UIManager.Instance.UpdateUnitUIInfo(this);
        actionIndicator.gameObject.SetActive(true);
        isMovementFinished = true;
    }

    public void UpdateMovement(int amount)
    {
        if (movementBase == 0)
        {
            return;
        }
        else if (amount < 0 && movementBase < -amount)
        {
            movementLeft = 1;
        }
        else
        {
            movementLeft += amount;
        }
    }

    // Updates the position of the destination according to its coordinates
    public void UpdatePosition()
    {
        goalPosition = hexComponent.transform.position;
    }
    /// <summary>
    /// Moves unit to target hex transform position.
    /// Clears starting hex from unit info and sets info into target hex.
    /// </summary>
    /// <param name="targetHex">Hex we want to move to (it's transform position).</param>
    public bool Move(HexComponent targetHex)
    {
        //clears current hex for unit movement
        if (targetHex == null)
        {
            Debug.Log(name + " is trying to move to null hex!");
            lastActionComplete = true;
            return false;
        }
        if (targetHex == hexComponent)
        {
            Debug.Log(name + " is trying to move to hex he is standing on!");
            lastActionComplete = true;
            return false;
        }
        // hexComponent.ClearObjectInfo(this);
        List<HexComponent> path = HexAccessor.HexMovementPath(hexComponent, targetHex, this);
        if (path == null || path.Count != 0)
        {
            isMovementFinished = false;
            StartCoroutine(MovementRoutine(path));

        }
        else
        {
            lastActionComplete = true;
            Debug.Log(name + " couldn't move!");
        }

        //targetHex.AddObjectInfo(this);

        // hexComponent = targetHex;
        // hex = hexComponent.hex;

        UIManager.Instance.UpdateUnitUIInfo(this);
        return true;
    }

    public void Move(List<HexComponent> hexPath)
    {
        //clears current hex for unit movement
        //Debug.Log(this.name + " moving to hex Q: " + targetHex.hex.Q + "  R: " + targetHex.hex.R + "  S: " + targetHex.hex.S);

        hexComponent.ClearObjectInfo(this);
        //List<HexComponent> path = HexAccessor.HexMovementPath(hexComponent, targetHex, this);
        isMovementFinished = false;
        StartCoroutine(MovementRoutine(hexPath));

        hexPath.Last().AddObjectInfo(this);
        hexComponent = hexPath.Last();
        hex = hexComponent.hex;

        UIManager.Instance.UpdateUnitUIInfo(this);
    }


    public override void Die()
    {
        base.Die();
    }
    public void CheckEndTurn()
    {
        if (movementLeft <= 0 && actionDone == true)
            turnEnded = true;
    }

    public void EndTurn()
    {
        ActionPerformed();
        lastActionComplete = true;
        movementLeft = 0;
        turnEnded = true;
    }

    virtual public void TurnStartRecovery()
    {
        foreach (Ability ability in abilityList.abilities)
        {
            if (ability.cooldownLeft > 0) { ability.cooldownLeft -= 1; }
            if (ability.isPassive)
            {
                ability.Use();
            }
        }
        ActionRestored();
        turnEnded = false;
        movementLeft = movementBase;
        hexFinder.VerifyHexComponent();
    }

    IEnumerator MovementRoutine(List<HexComponent> hexPath)
    {
        float waitTime = 0.05f;
        if (hexPath == null || hexPath.Count <= 0)
        {
            Debug.Log(name + " has 0 hexes in his path!  ");
            yield break;

        }
        Debug.Log("MOVING " + name + " path has " + hexPath.Count + "  hexes!  ");
        HexComponent previousHex = hexComponent;
        foreach (HexComponent hexComp in hexPath)
        {
            //Move(hexComp);
            //if(previousHex == hexComponent)
            //{
            //    hexComponent.ClearObjectInfo(this);
            //    hexComp.AddObjectInfo(this);
            //}
            //else
            //{
            previousHex.ClearObjectInfo(this);
            hexComp.AddObjectInfo(this);
            //  }
            isMoving = true;
            goalPosition = hexComp.transform.position;
            transform.DOLookAt(goalPosition, waitTime);
            while (isMoving)
            {
                if (Vector3.Distance(this.transform.position, goalPosition) > 0.01f)
                {
                    isMoving = true;
                    Vector3 newPosition = Vector3.MoveTowards(this.transform.position, goalPosition, speed);
                    this.transform.position = newPosition;
                }
                else
                {
                    isMoving = false;
                }
                yield return null;
            }
            UpdateMovement(-1);
            //UIManager.Instance.UpdateUnitUIInfo(this);
            OnStatusChanged();
            previousHex = hexComp;
            yield return new WaitForSeconds(waitTime);
        }
        Debug.Log(name + " finished his movement to " + hexPath[hexPath.Count-1].name + "  hex!  ");
        isMovementFinished = true;
        lastActionComplete = true;
        OnStatusChanged();
        yield return null;
    }

    protected void OnDestroy()
    {
        if (hexComponent != null)
        {
            hexComponent.mapObject = null;
            hexComponent.hex.hasObject = false;

        }
        if (factionID == FACTION_ID.PLAYER)
        {
            UnitManager.Instance.RemovePlayerUnit(this);
        }
        else if (factionID == FACTION_ID.ENEMY)
        {
            UnitManager.Instance.RemoveEnemyUnit(this);
        }
    }

    public void OnStatusChanged()
    {
        EventHandler handler = StatusChanged;
        handler?.Invoke(this, EventArgs.Empty);
    }

    public void OnAnimationNeeded()
    {
        EventHandler handler = AbilityUsed;
        handler?.Invoke(this, EventArgs.Empty);
    }
}
