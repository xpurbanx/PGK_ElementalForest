﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainTree : Unit
{
    public int seedLimit;
    public int seedUsed;

    public GameObject seedObj;

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
    }
    protected override void Start()
    {
        base.Start();

        TurnStartRecovery();
    }

    public void spawnSeed(HexComponent hex)
    {
        updateSeedCount();
        if (seedLimit > seedUsed)
        {
            hex.SpawnObject(seedObj);
            updateSeedCount();
        }
        else
        {
            Debug.Log(seedUsed.ToString() + "in use. Cant use more seeds");
        }
            
    }

    public void updateSeedCount()
    {
        seedUsed = UnitManager.Instance.playerUnits.Count;
    }

    public override void TurnStartRecovery()
    {
        base.TurnStartRecovery();
        if (currentHealth == 0 && TurnManager.Instance.turnCounter < 4)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        //
    }

    private void OnDestroy()
    {
        Debug.Log("Main tree destroyted");
        UnitManager.Instance.mainTree = false;
    }
}
