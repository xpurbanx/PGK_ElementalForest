﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Goblin : Unit
{
    //public int health;
    private IEnumerator decisionMaking;
    private Coroutine currentDecisionRoutine;
    private float maxTurnTimer = 1f;
    public float currentTurnTimer = 0f;
    public bool isTakingTurn = false;
    public OBJECT_TYPE preferredTarget;
    public List<FEATURE_TYPE> breakableFeatures;
    HexComponent targetUnit;
    HexComponent targetFeature;
    public bool canReachTarget = true;
    public bool isFocusingMapFeatures = false;
    public int viewRange = 1;
    protected override void Awake()
    {
        base.Awake();
        decisionMaking = DecisionRoutine();
    }

    protected override void Start()
    {
        base.Start();
        TurnStartRecovery();
        GetHexBelow();
    }


    public IEnumerator DecisionRoutine()
    {
        while (GameManager.Instance.isGameActive)
        {
            yield return new WaitForSeconds(0.1f);
            if (TurnManager.Instance.turnType == TurnManager.TurnType.Enemies && isTakingTurn == true)
            {


                if (movementLeft > 0 && canReachTarget)
                    targetUnit = ScanForEnemy(movementLeft + viewRange);
                else
                {
                    Debug.Log(name + " couldn't reach preffered target!");
                    targetUnit = ScanForEnemy(1);
                }

                targetFeature = null;
                if (targetUnit != null)
                {
                    transform.LookAt(targetUnit.transform);

                    if ((int)HexAccessor.Distance(targetUnit.hex, hex) > 1 && lastActionComplete == true)
                    {
                        MoveCloseToTarget(targetUnit);
                    }

                    if ((int)HexAccessor.Distance(targetUnit.hex, hex) == 1 && actionDone == false && lastActionComplete == true)
                    {
                        Debug.Log(name + " is attacking target " + targetUnit.mapObject.name);
                        AttackTarget(targetUnit.mapObject);
                    }

                }
                else if (isFocusingMapFeatures && breakableFeatures.Count > 0)
                {
                    targetFeature = ScanForFeature(movementLeft + viewRange);
                    if (targetFeature != null && (int)HexAccessor.Distance(targetFeature.hex, hex) > 1 && lastActionComplete == true)
                    {
                        MoveCloseToFeature(targetFeature);
                    }
                    //yield return null;
                }
                else if (lastActionComplete == true && movementLeft > 0)
                {
                    RandomMove();
                    //yield return null;
                }
                //else if (lastActionComplete == true)
                //{
                //    RandomMove();
                //    //yield return null;
                //}
                //scan with attack range, for now range == 1
                bool isTargetInRange = false;
                bool isFeatureInRange = false;
                if (targetUnit != null)
                {
                    if (HexAccessor.Distance(targetUnit.hex, hex) <= 1)
                    {

                        isTargetInRange = true;
                    }
                }

                if (breakableFeatures.Count > 0 && targetFeature == null)
                {
                    Debug.Log(name + " can break features! ");
                    ChooseHexFeatureToAttack();
                }

                if (targetFeature != null)
                {
                    if (HexAccessor.Distance(targetFeature.hex, hex) <= 1)
                        isFeatureInRange = true;
                }

                if (targetFeature != null && isFeatureInRange && !isTargetInRange)
                {
                    Debug.Log(name + " has target feature " + targetFeature.mapFeature.name);
                    if ((int)HexAccessor.Distance(targetFeature.hex, hex) == 1 && actionDone == false && lastActionComplete == true)
                    {
                        Debug.Log(name + " is attacking target " + targetFeature.mapFeature.name);
                        AttackTarget(targetFeature.mapFeature);
                    }
                    else
                    {
                        Debug.Log(name + " could not attack " + targetFeature.mapFeature.name +
                            " \ndistance " + HexAccessor.Distance(targetFeature.hex, hex).ToString()
                            + "\naction done " + actionDone
                            + "\n lastActionComplete  " + lastActionComplete);
                    }
                }

                if (movementLeft <= 0 && isTargetInRange == false && isFeatureInRange == false || actionDone)
                {
                    //can't possibly reach player now
                    TurnFinishedGoblin();
                    yield return null;
                }
                if (currentTurnTimer > maxTurnTimer)
                {
                    Debug.Log("Max turn timer reached, skipping goblin turn!");
                    TurnFinishedGoblin();
                    yield return null;
                }

                if (targetUnit != null)
                    transform.LookAt(targetUnit.transform);
                Debug.Log(name + " decision iteration done,\n lastActionComplete = " + lastActionComplete +
                    "\nmovement left = " + movementLeft.ToString() +
                    "\nactionDone = " + actionDone);

            }
        }

    }

    public void StartMakingDecisions()
    {
        Debug.Log(name + " is starting turn!");
        if (hexComponent == null)
        {
            //GetHexBelow();
            hexComponent.AddObjectInfo(this);
        }
        currentDecisionRoutine = StartCoroutine(decisionMaking);
    }
    public void StopMakingDecisions()
    {

        if (currentDecisionRoutine != null)
            StopCoroutine(decisionMaking);
    }

    public void TurnFinishedGoblin()
    {
        Debug.Log(name + " is finishing turn!");
        ActionPerformed();
        turnEnded = true;
        isTakingTurn = false;
        StopMakingDecisions();
        lastActionComplete = true;
    }

    public void AttackTarget(MapObject target)
    {
        lastActionComplete = false;
        if (target == null)
        {
            Debug.LogError("Goblin is trying to attack NULL target!");
            return;
        }
        foreach (Ability ability in abilityList.abilities)
        {
            if (ability is AAtack)
            {
                Debug.Log("Goblin attacked " + target.name + "!");
                UIManager.Instance.AddBattleLog("Goblin attacked " + target.nameObject + "!");
                ability.GetComponent<AAtack>().Act(target);
                actionDone = true;

                //goblins after attack dont do anything more
                movementLeft = 0;
            }
        }
    }

    public void RandomMove()
    {
        Debug.Log(name + " is moving randomly!");
        lastActionComplete = false;
        isMoving = true;

        List<HexComponent> availableHexes = HexAccessor.GetMovementHexes(this);
        availableHexes = HexAccessor.CheckForObjects(availableHexes);
        HexComponent mainTreeHex = UnitManager.Instance.mainTreeObject.hexComponent;
        if (availableHexes.Count > 0)
        {
            HexComponent target = availableHexes[Random.Range(0, availableHexes.Count)];
            if (target == null)
            {
                Debug.LogError("No target hex!");
                return;
            }
            else if (hex == null)
            {
                Debug.LogError("No MapObject hex!");
                return;
            }
            else if (target.mapObject != null)
            {
                Debug.Log("Target hex has object on it!");
                return;
            }
            else
            {
                target = ChooseHexCloserToTree(availableHexes);
                Move(target);
                //TurnFinishedGoblin();
            }

        }
        //cuz have no actions yet
        //TurnFinishedGoblin();
    }

    private HexComponent ScanForEnemy(int dist)
    {
        List<HexComponent> seenHexes = HexAccessor.GetHexesAround(hexComponent, dist);
        List<MapObject> enemyUnits = new List<MapObject>();
        MapObject preferredUnit = null;
        MapObject closestUnit = null;
        HexComponent chosenHex = null;
        HexComponent validHex = null;
        foreach (HexComponent hex in seenHexes)
        {
            if (hex.mapObject != null)
            {
                if (hex.mapObject.factionID == MapObject.FACTION_ID.PLAYER)
                {
                    if (!enemyUnits.Contains(hex.mapObject))
                        enemyUnits.Add(hex.mapObject);
                }
            }
        }
        if (enemyUnits.Count == 0)
            return null;
        foreach (MapObject unit in enemyUnits)
        {
            if (unit.objectType == preferredTarget)
            {
                if (preferredUnit == null)
                    preferredUnit = unit;
                //if there is closer preferred target
                else if (HexAccessor.Distance(hexComponent.hex, unit.hexComponent.hex) < HexAccessor.Distance(hexComponent.hex, preferredUnit.hexComponent.hex))
                {
                    preferredUnit = unit;
                }
            }
            else
            {
                if (closestUnit == null)
                    closestUnit = unit;
                //if there is closer preferred target
                else if (HexAccessor.Distance(hexComponent.hex, unit.hexComponent.hex) < HexAccessor.Distance(hexComponent.hex, closestUnit.hexComponent.hex))
                {
                    closestUnit = unit;
                }
            }
            if (preferredUnit != null)
                validHex = preferredUnit.hexComponent;
            else
                validHex = closestUnit.hexComponent;

            if (validHex != null)
            {
                chosenHex = validHex;
            }
        }
        // Debug.Log(nameObject + " found it's target " + chosenHex.mapObject.nameObject);
        return chosenHex;
    }
    private HexComponent ScanForFeature(int dist)
    {
        List<HexComponent> seenHexes = HexAccessor.GetHexesAround(hexComponent, dist);
        List<MapObject> features = new List<MapObject>();
        MapFeature preferredFeature = null;
        MapFeature closestFeature = null;
        HexComponent chosenHex = null;
        HexComponent validHex = null;
        foreach (HexComponent hex in seenHexes)
        {
            if (hex.mapFeature != null)
            {
                if (breakableFeatures.Contains(hex.mapFeature.featureType))
                {
                    if (!features.Contains(hex.mapFeature))
                        features.Add(hex.mapFeature);
                }
            }
        }
        if (features.Count == 0)
            return null;
        foreach (MapFeature feature in features)
        {
            if (feature.objectType == preferredTarget)
            {
                if (preferredFeature == null)
                    preferredFeature = feature;
                //if there is closer preferred target
                else if (HexAccessor.Distance(hexComponent.hex, feature.hexComponent.hex) < HexAccessor.Distance(hexComponent.hex, preferredFeature.hexComponent.hex))
                {
                    preferredFeature = feature;
                }
            }
            else
            {
                if (closestFeature == null)
                    closestFeature = feature;
                //if there is closer preferred target
                else if (HexAccessor.Distance(hexComponent.hex, feature.hexComponent.hex) < HexAccessor.Distance(hexComponent.hex, closestFeature.hexComponent.hex))
                {
                    closestFeature = feature;
                }
            }
            if (preferredFeature != null)
                validHex = preferredFeature.hexComponent;
            else
                validHex = closestFeature.hexComponent;

            if (validHex != null)
            {
                chosenHex = validHex;
            }
        }
        // Debug.Log(nameObject + " found it's target " + chosenHex.mapObject.nameObject);
        return chosenHex;
    }
    private HexComponent ChooseHexCloserToTree(List<HexComponent> hexes)
    {
        HexComponent closerToTree = hexes[0];
        HexComponent mainTreeHex = UnitManager.Instance.mainTreeObject.hexComponent;
        // List<HexComponent> shortestPath = HexAccessor.ShortestPathMovement(hexComponent, mainTreeHex, this);
        foreach (HexComponent hex in hexes)
        {
            //  if (!shortestPath.Contains(hex))
            //      continue;
            if ((int)HexAccessor.Distance(hex.hex, mainTreeHex.hex) < (int)HexAccessor.Distance(closerToTree.hex, mainTreeHex.hex))
            {
                closerToTree = hex;
            }
        }
        Debug.Log(name + " found closer hex to the tree  on hex " + closerToTree.name);
        return closerToTree;

    }

    /// <summary>
    /// Checks for hex with map feature to attack
    /// </summary>
    /// <param name="hexes">Hexes to scan for features</param>
    /// <returns>Hex with target feature</returns>
    private bool ChooseHexFeatureToAttack()
    {
        //List<HexComponent> hexes = HexAccessor.GetHexesAround(hexComponent, movementLeft);
        List<HexComponent> hexes = HexAccessor.GetHexesAround(hexComponent, 1);
        //availableHexes = HexAccessor.CheckForObjects(availableHexes);

        //means that there is no free space near target
        //scan for for breakable objects

        List<HexComponent> featureHexes = HexAccessor.CheckForFeatures(hexes, breakableFeatures);
        if (featureHexes.Count == 0)
        {
            Debug.Log(name + " sees no features in his range ");
            return false;
        }
        HexComponent closerToTarget = featureHexes[0];
        List<HexComponent> pathToFeature;
        foreach (HexComponent hex in featureHexes)
        {
            if (targetUnit != null)
            {
                if ((int)HexAccessor.Distance(hex.hex, targetUnit.hex) < (int)HexAccessor.Distance(closerToTarget.hex, targetUnit.hex))
                {

                    //check if path to move next to feature exists
                    // pathToFeature = HexAccessor.HexMovementPath(hexComponent, hex, this);
                    //if (pathToFeature.Count != HexAccessor.Distance(hex.hex, hexComponent.hex))
                    //     continue;

                    closerToTarget = hex;
                }
            }

        }

        targetFeature = closerToTarget;
        //closerToTarget = HexAccessor.GetEmptyHexInRange(targetFeature, this);
        //if (closerToTarget != null)
        //{
        //    Debug.Log(name + " moves closer to feature " + targetFeature.mapObject.name);
        //    isMoving = true;
        //    lastActionComplete = false;
        //    Move(closerToTarget);
        //    return true;
        //}
        //else
        //{
        //    Debug.Log(name + " could not move closer to feature " + targetFeature.mapObject.name);
        //    return false;

        //}
        return false;
    }

    private HexComponent ChooseHexCloserToTarget(List<HexComponent> hexes, HexComponent target)
    {
        if (hexes.Count == 0)
        {
            //Debug.Log(name + " hexes are null when trying to get closer to" + target.mapObject.name);
            return null;
        }
        HexComponent closerToTarget = hexComponent;
        //Debug.Log(name + " first hex closer to target " + target.mapObject.name + " is " + closerToTarget.name);
        foreach (HexComponent hex in hexes)
        {
            if ((int)HexAccessor.Distance(hex.hex, target.hex) < (int)HexAccessor.Distance(closerToTarget.hex, target.hex))
            {
                closerToTarget = hex;
            }
        }
        //Debug.Log(name + " found closer hex to target  " + target.mapObject.name + " on hex " + closerToTarget.name);
        return closerToTarget;

    }
    private void MoveCloseToTarget(HexComponent hx)
    {
        Debug.Log(name + " is trying to move next to target " + hx.mapObject.name + " on hex " + hx.name);
        if (movementLeft == 0)
        {
            Debug.Log(name + " has no movement left! ");
            return;
        }
        isMoving = true;
        lastActionComplete = false;

        List<HexComponent> availableHexes = HexAccessor.GetMovementHexes(this);
        availableHexes = HexAccessor.CheckForObjects(availableHexes);

        List<HexComponent> borderingHexes = HexAccessor.GetHexesAround(hx, 1);
        borderingHexes = FilterMovementHexes(borderingHexes);
        List<HexComponent> intersection = HexAccessor.Intersection(availableHexes, borderingHexes);

        bool moved = false;
        int triesAmount = 0;
        while (intersection.Count > 0)
        {
            HexComponent target = intersection[Random.Range(0, intersection.Count)];
            if (target.mapObject != null)
            {
                //Debug.Log("Target hex has object on it!");
                intersection.Remove(target);
                continue;
            }

            if (target != null)
            {
                moved = true;
                Debug.Log(name + " could move next to " + hx.mapObject.name + " on hex " + target.name);
                canReachTarget = true;
                // isMoving = true;
                // lastActionComplete = false;
                Move(target);
                return;
            }
            triesAmount++;
            if (triesAmount > 8)
            {
                Debug.LogError("was in infinite loop");
                break;
            }
        }
        Debug.Log(name + " couldn't move next to " + hx.mapObject.name);
        canReachTarget = false;
        //availableHexes = HexAccessor.GetHexesAround(hexComponent, movementLeft+1);
        ////availableHexes = HexAccessor.CheckForObjects(availableHexes);

        ////means that there is no free space near target
        ////scan for for breakable objects
        //if (breakableFeatures.Count > 0)
        //{
        //    Debug.Log(name + " can break features! ");
        //    moved = ChooseHexFeatureToAttack(availableHexes);
        //}
        availableHexes = HexAccessor.GetMovementHexes(this);
        availableHexes = HexAccessor.CheckForObjects(availableHexes);
        if (!moved)
        {
            Debug.Log(name + " is trying to get closer to " + hx.mapObject.name);
            HexComponent tmp = ChooseHexCloserToTarget(availableHexes, hx);
            if (tmp != null)
            {
                // isMoving = true;
                //  lastActionComplete = false;
                Move(tmp);
                //canReachTarget = false;
                return;

            }
        }
        else if (!moved && !canReachTarget)
        {
            Debug.Log(name + " can't get closer to " + hx.mapObject.name + ", moving randomly");
            RandomMove();
        }
        //if ((int)HexAccessor.Distance(hx.hex, hex) == 1 && actionDone == false)
        //{
        //    AttackTarget(hx.mapObject);
        //}
        //cuz have no actions yet
        //TurnFinishedGoblin();
    }

    private void MoveCloseToFeature(HexComponent hx)
    {
        Debug.Log(name + " is trying to move next to target " + hx.mapFeature.name + " on hex " + hx.name);
        if (movementLeft == 0)
        {
            Debug.Log(name + " has no movement left! ");
            return;
        }
        isMoving = true;
        lastActionComplete = false;

        List<HexComponent> availableHexes = HexAccessor.GetMovementHexes(this);
        availableHexes = HexAccessor.CheckForObjects(availableHexes);

        List<HexComponent> borderingHexes = HexAccessor.GetHexesAround(hx, 1);
        borderingHexes = FilterMovementHexes(borderingHexes);
        List<HexComponent> intersection = HexAccessor.Intersection(availableHexes, borderingHexes);

        bool moved = false;
        int triesAmount = 0;
        while (intersection.Count > 0)
        {
            HexComponent target = intersection[Random.Range(0, intersection.Count)];
            if (target.mapFeature != null)
            {
                intersection.Remove(target);
                continue;
            }

            if (target != null)
            {
                moved = true;
                Debug.Log(name + " could move next to " + hx.mapFeature.name + " on hex " + target.name);
                canReachTarget = true;
                Move(target);
                return;
            }
            triesAmount++;
            if (triesAmount > 8)
            {
                Debug.LogError("was in infinite loop");
                break;
            }
        }
        Debug.Log(name + " couldn't move next to " + hx.mapFeature.name);
        canReachTarget = false;
        availableHexes = HexAccessor.GetMovementHexes(this);
        availableHexes = HexAccessor.CheckForObjects(availableHexes);
        if (!moved)
        {
            Debug.Log(name + " is trying to get closer to " + hx.mapFeature.name);
            HexComponent tmp = ChooseHexCloserToTarget(availableHexes, hx);
            if (tmp != null)
            {
                Move(tmp);
                return;

            }
        }
        else if (!moved && !canReachTarget)
        {
            Debug.Log(name + " can't get closer to " + hx.mapFeature.name + ", moving randomly");
            RandomMove();
        }
    }
}
