﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elemental : Unit
{
    // public int mana;
    // Start is called before the first frame update
    // needed check to stop spawning seeds when exiting game
    bool isQuitting = false;
    public ELEMENTAL_TYPE elementalType;

    protected override void Awake()
    {
        base.Awake();
        transformsOnDeath = true;
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Die()
    {
        hexComponent.SpawnObject(MapObjectPrefabs.Instance.GetObjectPrefab(OBJECT_TYPE.Seed));
        base.Die();
    }
    // Update is called once per frame
    //void Update()
    //{

    //}
}
