﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ANIMATION_TYPE
{
    IDLE,
    MOVE,
    ATTACK,
    DAMAGED,
    CASTING
}


// 
[RequireComponent(typeof(MapObject))]
public class UnitController : MonoBehaviour
{
    public Unit unit;
    public GameObject selectionMarker;

    public Animator animator;

    public string attackTrigger = "attack1";
    public string damageTrigger = "damage";
    public string moveBool = "walk";
    public string idleBool = "idle01";

    // True if the object is currently selected
    [HideInInspector]
    public bool currentlyIsSelected = false;
    private void OnEnable()
    {
        if (unit != null)
        {
            unit.AbilityUsed += OnAnimationNeeded;
        }
    }
    private void OnDisable()
    {
        if (unit != null)
        {
            unit.AbilityUsed -= OnAnimationNeeded;
        }
    }
    private void Awake()
    {
        unit = GetComponent<Unit>();
    }

    private void Start()
    {
        selectionMarker.SetActive(false);
    }

    // Called when the object is selected
    // Makes the arrow above object visible
    public void Select()
    {
        selectionMarker.SetActive(true);
        currentlyIsSelected = true;
    }

    // Called when the object gets deselected
    // Hides the arrow above
    public void Deselect()
    {
        selectionMarker.SetActive(false);
        currentlyIsSelected = false;
    }

    public void OnAnimationNeeded(object sender, EventArgs e)
    {
       // Debug.Log("Animation should play. Type: " + unit.currentAnim.ToString());
        if (animator != null)
        {
            switch (unit.currentAnim)
            {
                case ANIMATION_TYPE.ATTACK:
                    animator.SetTrigger(attackTrigger);
                    //animationContainer.Play(attackAnimName, PlayMode.StopAll);
                   // Debug.Log("Playin attack!");
                    break;
                case ANIMATION_TYPE.DAMAGED:
                    animator.SetTrigger(damageTrigger);
                    //animationContainer.Play(damageAnimName);
                    break;
                case ANIMATION_TYPE.CASTING:

                    break;
                case ANIMATION_TYPE.MOVE:
                    animator.SetBool(moveBool, true);
                    animator.SetBool(idleBool, false);
                    //animationContainer.Play(moveAnimName);
                    break;
                case ANIMATION_TYPE.IDLE:
                    animator.SetBool(moveBool, false);
                    animator.SetBool(idleBool, true);
                    //animationContainer.Play(idleAnimName);
                    break;
                default:

                    break;
            }

            //animationContainer.PlayQueued(idleAnimName, QueueMode.CompleteOthers);
        }

    }

}
