﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : Unit
{


    public GameObject chargedParticles;
    protected override void Start()
    {
        base.Start();
        manaUser = true;
        EndTurn();
        if (manaCurrent != manaMax)
        {
            chargedParticles.SetActive(false);

        }
        else
        {
            chargedParticles.SetActive(true);
        }
    }

    public void Killed()
    {
        UnitManager.Instance.elementalLimit--;
        // Die();
    }

    public void Transported()
    {
        //to offset Killed invoked in Die
        UnitManager.Instance.elementalLimit++;
        Die();
    }

    public override void Die()
    {
        Killed();
        UIManager.Instance.AddBattleLog(nameObject + " was shattered by enemy, decrease max elemental count!");
        base.Die();
    }

    public void ChangeMana(int amount)
    {
        manaCurrent += amount;
        floatingText.ShowManaReceived(amount);
        if (manaCurrent > manaMax)
        {
            manaCurrent = manaMax;
        }

        if (manaCurrent < 0)
        {
            manaCurrent = 0;
        }

        if (manaCurrent == manaMax)
        {
            chargedParticles.SetActive(true);
            UIManager.Instance.AddBattleLog(nameObject + " is fully charged!");
        }
        else if (manaCurrent < manaMax)
        {
            UIManager.Instance.AddBattleLog(nameObject + " is charging");
            chargedParticles.SetActive(false);
        }
    }

}
