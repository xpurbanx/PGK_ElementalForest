﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TutorialWindow : MonoBehaviour
{
    public List<TutorialPage> pages;

    public int currentPage = 0;

    // Start is called before the first frame update
    void Start()
    {
        pages = GetComponentsInChildren<TutorialPage>().ToList();
        CloseAllPages();
        OpenPage(0);
    }

    public void OpenPage(int pageNumber)
    {
        if (pageNumber > pages.Count || pageNumber < 0)
            return;

        CloseAllPages();
        pages[pageNumber].EnableContent();
    }

    public void CloseAllPages()
    {
        foreach(TutorialPage page in pages)
        {
            page.DisableContent();
        }
    }

    public void NextPage()
    {
        if (currentPage < pages.Count-1)
        {
            pages[currentPage].DisableContent();
            pages[currentPage+1].EnableContent();
            currentPage++;
        }
    }

    public void PreviousPage()
    {
        if (currentPage > 0)
        {
            pages[currentPage].DisableContent();
            pages[currentPage - 1].EnableContent();
            currentPage--;
        }
    }

    public void CloseButton()
    {
        this.gameObject.SetActive(false);
    }
}
