﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectPrefabs : MonoBehaviour
{
    public static MapObjectPrefabs Instance;
    public List<GameObject> objects = new List<GameObject>();


    private void Awake()
    {
        Instance = this;
    }

    public GameObject GetObjectPrefab(OBJECT_TYPE type)
    {
        foreach(GameObject prefab in objects)
        {
            if (prefab.GetComponent<MapObject>().objectType == type)
                return prefab;
        }

        return null;
    }
}
