﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void OpenInstructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void PlayLevel(int n)
    {
        SceneManager.LoadScene("Map"+n);
    }

    public void PlayTutorial(int n)
    {
        SceneManager.LoadScene("Tutorial" + n);
    }
}
