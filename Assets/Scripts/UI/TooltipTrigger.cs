﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string header;

    [Multiline()]
    public string content;

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        transform.DOLookAt(transform.position,0).SetDelay(1).OnComplete(() =>
        {
            TooltipSystem.Show(content, header);

        });

    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        DOTween.KillAll();
        TooltipSystem.Hide();


    }
}
