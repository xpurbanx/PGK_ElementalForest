﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(HexComponent))]
public class TooltipTriggerTile : TooltipTrigger, IPointerEnterHandler, IPointerExitHandler
{
    HexComponent hexComponent;

    [Multiline()]
    public string grassContent;

    [Multiline()]
    public string waterContent;

    [Multiline()]
    public string earthContent;

    [Multiline()]
    public string barrenContent;


    private void Awake()
    {
        hexComponent = GetComponent<HexComponent>();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {

        switch (hexComponent.hex.TerrainType)
        {
            case TERRAIN_TYPE.GRASS:
                header = "Grass";
                content = grassContent;
                break;
            case TERRAIN_TYPE.BARREN:
                header = "Barren";
                content = barrenContent;
                break;
            case TERRAIN_TYPE.WATER:
                header = "Water";
                content = waterContent;
                break;
            case TERRAIN_TYPE.EARTH:
                header = "Earth";
                content = earthContent;
                break;
            default:
                break;
        }
        base.OnPointerEnter(eventData);

    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);


    }
}

