﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class UnitTabInfo : MonoBehaviour
{
    public GameObject infoPanel;
    Canvas canvas;
    Unit unit;
    [SerializeField]
    Image panel;

    [SerializeField]
    Color playerColor;

    [SerializeField]
    Color enemyColor;

    public Transform camTransform;

    Quaternion originalRotation;

    public TextMeshProUGUI unitName;
    public TextMeshProUGUI health;
    public TextMeshProUGUI action;
    public TextMeshProUGUI movement;

    public Image actionYes;
    public Image actionNo;
    //public TextMeshPro health;

    void Start()
    {
        originalRotation = infoPanel.transform.rotation;
        camTransform = UICamera.Instance.uiCamera.transform;
        canvas = GetComponent<Canvas>();
        unit = GetComponentInParent<Unit>();

        if(unit.factionID == MapObject.FACTION_ID.ENEMY)
        {
            panel.color = enemyColor;
        }
        else
        {
            panel.color = playerColor;
        }

        canvas.worldCamera = UICamera.Instance.uiCamera;
    }

    // Update is called once per frame
    void Update()
    {
        infoPanel.transform.rotation = camTransform.rotation * originalRotation;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            infoPanel.SetActive(true);
            //infoPanel.transform.rotation = camTransform.rotation * originalRotation;
            UpdateInfoPanel();
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            infoPanel.SetActive(false);
        }
    }

    void UpdateInfoPanel()
    {
        unitName.text = unit.nameObject;
        health.text = unit.GetCurrentHealth().ToString() + " / " + unit.healthMax.ToString();
        if (unit.CanAct())
        {
            actionYes.enabled = true;
            actionNo.enabled = false;
        }
        else
        {
            actionYes.enabled = false;
            actionNo.enabled = true;
        }
        //action.text = unit.CanAct().ToString();
        movement.text = unit.movementLeft.ToString() + " / " + unit.movementBase.ToString();
    }
}
