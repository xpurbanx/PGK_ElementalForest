﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoordText : MonoBehaviour
{
    public TextMeshPro text;
    private HexComponent hexComponent;

    private void Awake()
    {
        text = GetComponent<TextMeshPro>();
        hexComponent = GetComponentInParent<HexComponent>();
    }

    private void OnEnable()
    {
        SetCoordText();
    }

    public void SetCoordText()
    {
        text.SetText("Q: " + hexComponent.hex.Q + "  R: " + hexComponent.hex.R + "\n\nS: " + hexComponent.hex.S);
    }

}
