﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

[RequireComponent(typeof(Canvas))]
public class FloatingText : MonoBehaviour
{
    public GameObject textHolder;
    public TextMeshProUGUI textDamaged;
    public TextMeshProUGUI textHealed;
    public TextMeshProUGUI textManaGained;
    Canvas canvas;
    MapObject mapObject;

    [SerializeField]
    Color damagedColor;

    [SerializeField]
    Color healedColor;

    public Transform camTransform;

    public Quaternion originalRotation;
    public Vector3 originalPosition;
    void Start()
    {
        originalPosition = textHolder.transform.localPosition;
        originalRotation = textHolder.transform.rotation;
        camTransform = UICamera.Instance.uiCamera.transform;
        canvas = GetComponent<Canvas>();
        // mapObject = GetComponentInParent<MapObject>();

        canvas.worldCamera = UICamera.Instance.uiCamera;
    }

    // Update is called once per frame
    void Update()
    {
        textHolder.transform.rotation = camTransform.rotation * originalRotation;
    }

    public void ShowDamageReceived(int damage)
    {
        //textHolder.transform.DOKill();
        GameObject floatingObject = Instantiate(this.gameObject, transform.position, transform.rotation);
        FloatingText tmp = floatingObject.GetComponent<FloatingText>();
        Destroy(floatingObject, 1.6f);
        tmp.originalPosition = originalPosition;
        tmp.originalRotation = textHolder.transform.rotation;
        //tmp.textHolder.transform.rotation = camTransform.rotation * originalRotation;
        tmp.textManaGained.gameObject.SetActive(false);
        if (damage >= 0)
        {

            tmp.textDamaged.gameObject.SetActive(true);
            tmp.textHealed.gameObject.SetActive(false);

            tmp.textDamaged.text = "-" + damage.ToString();
        }
        else
        {
            damage = damage * -1;
            tmp.textDamaged.gameObject.SetActive(false);
            tmp.textHealed.gameObject.SetActive(true);

            tmp.textHealed.text = "+" + damage.ToString();
        }
        tmp.textHolder.transform.localPosition = tmp.originalPosition;
        tmp.textHolder.transform.DOScale(0.4f, 0);
        tmp.textHolder.transform.DOLocalMoveY(tmp.textHolder.transform.localPosition.y + 100, 1.5f);
        tmp.textHolder.transform.DOScale(1.8f, 1.5f).OnComplete(() => { Destroy(tmp.gameObject); });
    }

    public void ShowManaReceived(int amount)
    {
        //textHolder.transform.DOKill();
        GameObject floatingObject = Instantiate(this.gameObject, transform.position, transform.rotation);
        FloatingText tmp = floatingObject.GetComponent<FloatingText>();
        Destroy(floatingObject, 1.6f);
        tmp.originalPosition = originalPosition;
        tmp.originalRotation = textHolder.transform.rotation;
        tmp.textManaGained.gameObject.SetActive(true);
        tmp.textDamaged.gameObject.SetActive(false);
        tmp.textHealed.gameObject.SetActive(false);
        if (amount < 0)
        {
            tmp.textManaGained.text = "-" + amount.ToString();
        }
        else
        {
            tmp.textManaGained.text = "+" + amount.ToString();
        }
        tmp.textHolder.transform.localPosition = tmp.originalPosition;
        tmp.textHolder.transform.DOScale(0.4f, 0);
        tmp.textHolder.transform.DOLocalMoveY(tmp.textHolder.transform.localPosition.y + 100, 1.5f);
        tmp.textHolder.transform.DOScale(1.8f, 1.5f).OnComplete(() => { Destroy(tmp.gameObject); });
    }

    void HideText()
    {
        textHolder.transform.localPosition = originalPosition;
        textHolder.transform.DOScale(0.7f, 0);
        textDamaged.gameObject.SetActive(false);
        textHealed.gameObject.SetActive(false);
    }
}
