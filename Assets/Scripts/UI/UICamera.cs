﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class UICamera : MonoBehaviour
{
    public Camera uiCamera;
    public static UICamera Instance;
    
    void Awake()
    {
        Instance = this;
        uiCamera = GetComponent<Camera>();
    }

    private void Update()
    {
        transform.position = Camera.main.transform.position;
        transform.rotation = Camera.main.transform.rotation;
    }

}
