﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPage : MonoBehaviour
{
    public GameObject content;
    
    public void EnableContent()
    {
        content.SetActive(true);
    }
    public void DisableContent()
    {
        content.SetActive(false);
    }
}
