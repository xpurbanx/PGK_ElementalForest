﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HexText : MonoBehaviour
{
    public TextMeshPro coordText;
    public TextMeshPro distanceText;
    private HexComponent hexComponent;

    private void Awake()
    {
        hexComponent = GetComponentInParent<HexComponent>();
    }

    private void OnEnable()
    {
        //SetCoordText();
    }

    public void SetCoordText()
    {
        //coordText.SetText("Q: " + hexComponent.hex.Q + "  R: " + hexComponent.hex.R + "\n\nS: " + hexComponent.hex.S);
        //coordText.SetText("");
    }

    public void SetDistanceText(int distance)
    {
        distanceText.SetText(distance.ToString());
    }
}
