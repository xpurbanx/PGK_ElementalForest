﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(HexComponent))]
public class HexEditor : Editor
{
    public override void OnInspectorGUI()
    {
        HexComponent myTarget = (HexComponent)target;

        if (GUILayout.Button("Set Hex to Earth"))
        {
            myTarget.hex.TerrainType = TERRAIN_TYPE.EARTH;
            myTarget.UpdateTerrain();
        }
    }
}
