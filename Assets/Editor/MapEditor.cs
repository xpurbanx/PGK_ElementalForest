﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Map))]
public class MapEditor : Editor
{
    bool mapExists = false;
    TERRAIN_TYPE currentTerrain = TERRAIN_TYPE.EARTH;
    HexComponent hexHit;

    public override void OnInspectorGUI()
    {
        Map myTarget = (Map)target;

        myTarget.hexTilePrefab = (GameObject)EditorGUILayout.ObjectField(myTarget.hexTilePrefab, typeof(GameObject), false);
        myTarget.hexHolder = (GameObject)EditorGUILayout.ObjectField(myTarget.hexHolder, typeof(GameObject), true);
        EditorGUILayout.LabelField("EnemyUnitHolder");
        myTarget.enemyUnitsHolder = (GameObject)EditorGUILayout.ObjectField(myTarget.enemyUnitsHolder, typeof(GameObject), true);
        EditorGUILayout.LabelField("PlayerUnitsHolder");
        myTarget.playerUnitsHolder = (GameObject)EditorGUILayout.ObjectField(myTarget.playerUnitsHolder, typeof(GameObject), true);
        EditorGUILayout.LabelField("MapFeaturesHolder");
        myTarget.mapFeaturesHolder = (GameObject)EditorGUILayout.ObjectField(myTarget.mapFeaturesHolder, typeof(GameObject), true);
        EditorGUILayout.LabelField("UnitPrefabs");
        myTarget.unitPrefabs = (MapObjectPrefabs)EditorGUILayout.ObjectField(myTarget.unitPrefabs, typeof(MapObjectPrefabs), true);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        myTarget.MatEarth = (Material)EditorGUILayout.ObjectField(myTarget.MatEarth, typeof(Material), false);
        myTarget.MatGrass = (Material)EditorGUILayout.ObjectField(myTarget.MatGrass, typeof(Material), false);
        myTarget.MatWater = (Material)EditorGUILayout.ObjectField(myTarget.MatWater, typeof(Material), false);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        myTarget.mapName = EditorGUILayout.TextField("Map Name", myTarget.mapName);
        if (GUILayout.Button("Load Map"))
        {
            myTarget.LoadMap(myTarget.mapName);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        if (GUILayout.Button("Destory Map"))
        {
            myTarget.DestroyMap();
            mapExists = false;
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        //EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
        if (GUILayout.Button("Update Map Visuals"))
        {
            myTarget.UpdateAllHexVisuals();
        }
        if (GUILayout.Button("Earth Mat"))
        {
            currentTerrain = TERRAIN_TYPE.EARTH;
        }
        if (GUILayout.Button("Grass Mat"))
        {
            currentTerrain = TERRAIN_TYPE.GRASS;
        }
        if (GUILayout.Button("Water Mat"))
        {
            currentTerrain = TERRAIN_TYPE.WATER;
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }
    
    private void OnSceneGUI()
    {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        Event guiEvent = Event.current;
        if (guiEvent.type == EventType.MouseDrag && guiEvent.button == 0)
        {
            Debug.Log("Clicked!!");
            Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;



            if (Physics.Raycast(worldRay, out hitInfo, Mathf.Infinity))
            {
                if ((hexHit = hitInfo.collider.GetComponent<HexComponent>()) != null)
                {
                    hexHit.hex.TerrainType = currentTerrain;
                    hexHit.UpdateTerrain();
                    hexHit.map.UpdateAllHexVisuals();

                }


            }

        }

       //Event.current.Use();
    }
}
